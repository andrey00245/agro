$(document).ready(function () {

    $('.main-slider-inner').slick({
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        centerPadding: '50px',
        dots: false,
        responsive: [
            {
                breakpoint: 600,
                settings: {
                    centerMode: true,
                    slidesToShow: 1,
                },
            },
        ],
    })
    $('.burger-menu').click(() => {
        $('.burger-menu-wrapper').fadeIn()
    })
    $('.burger-menu-close').click(() => {
        $('.burger-menu-wrapper').fadeOut()
    })

    $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.slider-nav',
        infinite: true,
    })
    $('.slider-nav').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        focusOnSelect: true,
        asNavFor: '.slider-for',
        dots: false,
        infinite: true,
    })


    $('select[data-menu]').each(function () {
        let select = $(this),
            options = select.find('option'),
            menu = $('<div />').addClass('select-menu'),
            button = $('<div />').addClass('button'),
            list = $('<ul />'),
            arrow = $('<div class="select-arrow"/>').prependTo(button);
        options.each(function (i) {
            let option = $(this);
            // list.append($('<li />').text('Сортировать по: ' + option.text()));
            list.append($(`<li>Сортировать по: <span> ${option.text()} </span></li>`))
        });

        menu.css('--t', select.find(':selected').index() * -41 + 'px');

        select.wrap(menu);

        button.append(list).insertAfter(select);

        list.clone().insertAfter(button);

    });

    $(document).on('click', '.select-menu', function (e) {
        let menu = $(this);

        if (!menu.hasClass('open')) {
            menu.addClass('open');
        }

    });

    $(document).on('click', '.select-menu > ul > li', function (e) {

        let li = $(this),
            menu = li.parent().parent(),
            select = menu.children('select'),
            selected = select.find('option:selected'),
            index = li.index();

        menu.css('--t', index * -41 + 'px');
        selected.attr('selected', false);
        select.find('option').eq(index).attr('selected', true);

        menu.addClass(index > selected.index() ? 'tilt-down' : 'tilt-up');

        setTimeout(() => {
            menu.removeClass('open tilt-up tilt-down');
        }, 500);

    });

    $(document).click(e => {
        e.stopPropagation();
        if ($('.select-menu').has(e.target).length === 0) {
            $('.select-menu').removeClass('open');
        }
    })

    $('.faq-question').on("click", function () {
        const element = $(this).parent()
        if (element.hasClass('open')) {
            element.removeClass('open')
            element.find('.faq-answer').fadeOut()
        } else {
            element.addClass('open')
            element.find('.faq-answer').fadeIn()
        }
    });
});
