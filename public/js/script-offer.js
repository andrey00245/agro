let selectedLevel = "first";
let technicalActiveIndex = 0;
$(".levels-container").addClass(selectedLevel);
$(`.tab-wrapper.first`).fadeIn(500);
$(`.how-it-works-content.first`).fadeIn(500);

$(`.technical-item[data-index=${technicalActiveIndex}]`).addClass("active");
$(`.technical-image-item[data-index=${technicalActiveIndex}]`).fadeIn(500);

const removeClassHandler = () => {
  $(".levels-container").removeClass(selectedLevel);
  $(`.tab-wrapper.${selectedLevel}`).fadeOut(500);
  $(`.how-it-works-content.${selectedLevel}`).fadeOut(500);
};

const addClassHandler = (string) => {
  $(".levels-container").addClass(string);
  $(`.tab-wrapper.${string}`).fadeIn(1000);
  $(`.how-it-works-content.${string}`).fadeIn(500);
};

$(".level-item").on("click", function () {
  const newClassName = $(this).attr("data-number");
  if (newClassName !== selectedLevel) {
    removeClassHandler();
    addClassHandler(newClassName);
    selectedLevel = newClassName;
  }
});
$(".burger-menu").click(() => {
  $(".burger-menu-wrapper").fadeIn();
});
$(".burger-menu-close").click(() => {
  $(".burger-menu-wrapper").fadeOut();
});
const removeIndexItem = () => {
  $(`.technical-item[data-index=${technicalActiveIndex}]`).removeClass(
    "active"
  );
  $(`.technical-image-item[data-index=${technicalActiveIndex}]`).fadeOut(500);
};
const addIndexItem = (index) => {
  $(`.technical-item[data-index=${index}]`).addClass("active");
  $(`.technical-image-item[data-index=${index}]`).fadeIn(1000);
};
$(".technical-item").on("click", function () {
  const newActiveIndex = $(this).attr("data-index");
  if (newActiveIndex !== technicalActiveIndex) {
    removeIndexItem();
    addIndexItem(newActiveIndex);
    technicalActiveIndex = newActiveIndex;
  }
});
$(document).ready(function () {
    $(".link-to-section").on("click", function (event) {
        event.preventDefault();
        if ($(this).hasClass("hide-menu")) $(".burger-menu-wrapper").fadeOut();
        const id = $(this).attr("href"),
            top = $(id).offset().top;
        $("body,html").animate({ scrollTop: top }, 1000);
    });

    $('#contact-us').on('submit', function (e) {
        e.preventDefault();

        $(".errors").html('');

        let name = $('#contact_name').val(),
            email = $('#contact_email').val(),
            phone = $('#contact_phone').val(),
            description = $('#contact_description').val();

        $.ajax({
            type: 'POST',
            url: '/contact-us',
            data: {name: name, email: email, phone: phone, description: description},
            success: function (data) {
                    $('#contact_name').val('');
                    $('#contact_email').val('');
                    $('#contact_phone').val('');
                    $('#contact_description').val('');

                    $('.success').html('Запрос Отправлен');
            },
            error: function(xhr, status, error)
            {
                $.each(xhr.responseJSON.errors, function (key, item)
                {
                    $(".errors").append("<li class='alert alert-danger'>"+item+"</li>")
                });
            }
        });
    });
});


window.onscroll = function() {myFunction()};
const header = $('.header-wrapper.desktop')
const sticky = $('body').offset().top;
const pageWrapper = $('.page-inner-wrapper');
function myFunction() {
  if (window.pageYOffset > sticky) {
      header.addClass("sticky")
  } else {
      header.removeClass("sticky");
  }
}
