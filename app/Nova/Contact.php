<?php

namespace App\Nova;

use Ebess\AdvancedNovaMediaLibrary\Fields\Files;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Panel;
use Waynestate\Nova\CKEditor;

class Contact extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Contact::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    public static $group = 'Навигация';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    public function authorizedToDelete(Request $request)
    {
        return false;
    }

    public static function authorizedToCreate(Request $request)
    {
        return false;
    }

    public static function label()
    {
        return 'Контакты';
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')->sortable(),
            Text::make('Телефон', 'phone')->rules('required'),
            Text::make('Почта', 'email')->rules('required'),
            Text::make('Адрес', 'address')->rules('required'),
            Textarea::make('Карта', 'map')->rules('required'),
            Textarea::make('Карта всех участков', 'location'),
            Textarea::make('Текст на Главной', 'main_text'),
            CKEditor::make('Виноделие', 'vinomarking')->hideFromIndex(),
            Text::make('Whatsapp', 'whatsapp')->hideFromIndex(),
            Text::make('Instagram')->hideFromIndex(),
            Text::make('Youtube')->hideFromIndex(),
            Text::make('Телефон для Участков', 'land_phone')->hideFromIndex(),

            new Panel('Винодел Крым', [
                Text::make('Телефон', 'agro_phone')->nullable()->hideFromIndex(),
                Text::make('Email', 'agro_email')->nullable()->hideFromIndex(),
                Text::make('Адрес', 'agro_address')->nullable()->hideFromIndex(),
                Text::make('Whatsapp', 'agro_whatsapp')->nullable()->hideFromIndex(),
                Text::make('Youtube', 'agro_youtube')->nullable()->hideFromIndex(),
                Text::make('Instagram', 'agro_instagram')->nullable()->hideFromIndex(),
                Text::make('Telegram', 'agro_telegram')->nullable()->hideFromIndex(),
                Text::make('Facebook', 'agro_facebook')->nullable()->hideFromIndex(),

            ]),
            new Panel('Файлы', [
                Files::make('Pdf File', 'image'),
            ]),
        ];
    }
}
