<?php

namespace App\Nova;

use Ebess\AdvancedNovaMediaLibrary\Fields\Images;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Panel;

class Land extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Land::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    public static function label()
    {
        return 'Земельные Участки';
    }

    public static function singularLabel()
    {
        return 'Земельный участок';
    }

    public static $group = 'Земельные Участки';

    /**
     * Get the fields displayed by the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')->sortable(),
            Images::make('Изображение', 'image')->hideFromIndex()->rules('required'),
            Images::make('Галерея', 'gallery')->hideFromIndex(),
            BelongsTo::make('Расположение', 'countryLand', CountryLand::class),
            Text::make('Название', 'title')->rules('required'),
            Textarea::make('Описание', 'description')->rules('required'),
            Text::make('Площадь в га', 'area'),
            Number::make('Стоимость за га', 'cost'),
            Number::make('Стоимость за обьект', 'cost_for_object')->rules('required'),
            Textarea::make('Карта', 'map'),
            Text::make('Видео(ссылка)', 'video_url')->hideFromIndex(),
            Text::make('Название для Видео', 'video_title')->hideFromIndex(),
            Textarea::make('Описание для Видео', 'video_description'),
            Text::make('Телефон', 'phone')->nullable(),

            Boolean::make('Отображение', 'is_active'),


            new Panel('Мета Теги', [
                Textarea::make('Мета Тайл', 'meta_title'),
                Textarea::make('Мета Дискрипшин', 'meta_description'),
                Textarea::make('Мета Кейвордс', 'meta_keywords'),
            ]),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
