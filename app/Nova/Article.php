<?php

namespace App\Nova;

use App\ArticleCategory;
use Ebess\AdvancedNovaMediaLibrary\Fields\Files;
use Ebess\AdvancedNovaMediaLibrary\Fields\Images;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;

class Article extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Article::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    public static $group = 'Библиотека';

    public static function label()
    {
        return "Статьи";
    }

    public static function singularLabel()
    {
        return "Статью";
    }


    /**
     * Get the fields displayed by the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')->sortable(),
            BelongsTo::make('Категория','articleCategory',\App\Nova\ArticleCategory::class)->rules('required'),
            Text::make('Название', 'title')->rules('required'),
            Textarea::make('Описание', 'description')->rules('required'),
            Images::make('Изображение', 'image'),
            Files::make('Документ', 'file'),
        ];
    }
}
