<?php

namespace App\Nova;

use Ebess\AdvancedNovaMediaLibrary\Fields\Files;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Panel;

class Suggestion extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Suggestion::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    public static function label()
    {
        return 'Cпец. Предложения';
    }

    public static function singularLabel()
    {
        return 'Предложение';
    }

    public static $group = 'Навигация';


    /**
     * Get the fields displayed by the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')->sortable(),
            Text::make('Название', 'title')->rules('required'),
            Textarea::make('Описание', 'description'),
            Files::make('Файл', 'file')
                ->rules('required'),

//            new Panel('Мета Теги', [
//                Textarea::make('Мета Тайл', 'meta_title'),
//                Textarea::make('Мета Дискрипшин', 'meta_description'),
//                Textarea::make('Мета Кейвордс', 'meta_keywords'),
//            ]),
        ];
    }
}
