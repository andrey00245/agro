<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    public function categoryVideo()
    {
        return $this->belongsTo(CategoryVideo::class);
    }
}
