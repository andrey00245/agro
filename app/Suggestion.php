<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Suggestion extends Model implements HasMedia
{
    use InteractsWithMedia;

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb');
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('file')->singleFile();
    }

    public function setFileAttribute($file)
    {
        $this->clearMediaCollection();

        if ($file !== null) {
            $this->addMedia($file)->toMediaCollection('file');
        }
    }

    public function getFile()
    {
        return $this->getFirstMediaUrl('file') ?: null;
    }

    public function getShortDescription()
    {
        return strlen($this->description) > 400
            ?  mb_substr($this->description, 0, 400) . '...'
            : $this->description;
    }
}
