<?php

namespace App\Enums;

use MadWeb\Enum\Enum;

/**
 * @method static QuestionStatusEnum NEW()
 * @method static QuestionStatusEnum COMPLETED()
 */
final class QuestionStatusEnum extends Enum
{
    const __default = self::NEW;

    const NEW = 'новый';
    const COMPLETED = 'просмотрен';
}
