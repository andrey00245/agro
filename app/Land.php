<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Land extends Model implements HasMedia
{
    use InteractsWithMedia;

    protected $fillable = [
        'phone',
    ];

    public static function boot()
    {
        parent::boot();

        static::saving(function ($model) {
            $model->slug = Str::slug($model->title);
        });
    }

    public function countryLand()
    {
        return $this->belongsTo(CountryLand::class);
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')
            ->width(216)
            ->height(200);
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('image')->singleFile();
        $this->addMediaCollection('gallery');
    }

    public function setImageAttribute($image)
    {
        $this->clearMediaCollection();

        if ($image !== null) {
            $this->addMedia($image)->toMediaCollection('image');
        }
    }

    public function hasGallery()
    {
        return $this->hasMedia('gallery');
    }

    public function getGallery()
    {
        return $this->getMedia('gallery');
    }

    public function getMainImage()
    {
        return $this->getFirstMediaUrl('image') ?: null;
    }

    public function getShortTitle()
    {
        return strlen($this->title) > 25
            ? mb_substr($this->title, 0, 25) . '...'
            : $this->title;
    }

    public function getShortDescription()
    {
        return strlen($this->description) > 200
            ? mb_substr($this->description, 0, 200) . '...'
            : $this->description;
    }

    public function getPhoneAttribute($phone)
    {
        return $phone ?? Contact::first()->land_phone;
    }
}
