<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryVideo extends Model
{
    public function videos()
    {
        return $this->hasMany(Video::class);
    }
}
