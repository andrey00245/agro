<?php

namespace App;

use App\Enums\QuestionStatusEnum;
use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Boolean;

class UserQuestion extends Model
{
    protected $fillable = ['question', 'status'];

    protected $attributes = [
        'status' => QuestionStatusEnum::__default
    ];
}
