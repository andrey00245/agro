<?php

namespace App\Providers;

use App\Contact;
use App\Seo;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        View::composer(['layout.main-header'], function ($view) {
            $view->with('seo', Seo::where('url', "/")->first());
        });

        View::composer(['layout.other-header'], function ($view) {
            if ($seo = Seo::where('url', request()->path())->first()) {
                $view->with('seo', $seo);
            } else {
                $view->with('seo', Seo::where('url', "/")->first());
            }
        });

        View::composer(['special-offer'], function ($view) {
            if ($seo = Seo::where('url', request()->path())->first()) {
                $view->with('seo', $seo);
            } else {
                $view->with('seo', Seo::where('url', "/")->first());
            }
        });

        View::share([
            'contact' => Contact::first(),
        ]);
    }
}
