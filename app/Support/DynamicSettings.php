<?php

namespace App\Support;

use Spatie\Valuestore\Valuestore;

class DynamicSettings
{
    protected $store;

    public function __construct()
    {
        $this->store = Valuestore::make(
            config('nova-settings-tool.path', storage_path('app/settings.json'))
        );
    }

    public function getAboutSeo(): string
    {
        return $this->store->get('about_seo') ?: '#';
    }

    public function getFIO(): string
    {
        return $this->store->get('fio') ?: '#';
    }

    public function getPosition(): string
    {
        return $this->store->get('position') ?: '#';
    }

    public function getEmail()
    {
        return $this->store->get('email');
    }

    public function getPhone()
    {
        return $this->store->get('phone');
    }

    public function getDescription()
    {
        return $this->store->get('description');
    }
}
