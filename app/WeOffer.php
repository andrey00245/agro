<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class WeOffer extends Model implements HasMedia
{
    use InteractsWithMedia;

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('image')->singleFile();
    }

    public function setImageAttribute($image)
    {
        $this->clearMediaCollection();

        if ($image !== null) {
            $this->addMedia($image)->toMediaCollection('image');
        }
    }
    public function getImageAttribute($image)
    {
        if ($this->hasMedia('image') == null){
            return null;
        }

        return  $this->getFirstMedia('image')->getUrl();
    }
}
