<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CountryLand extends Model
{
    public function lands()
    {
        return $this->hasMany(Land::class);
    }
}
