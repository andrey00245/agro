<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Contact extends Model implements HasMedia
{
    use InteractsWithMedia;

    protected $table = 'contacts';

    protected $fillable = [
        'land_phone',
        'image',
        'agro_phone',
        'agro_email',
        'agro_address',
        'agro_whatsapp',
    ];

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('image')->singleFile();
    }

    public function setImageAttribute($image)
    {
        $this->clearMediaCollection();

        if ($image !== null) {
            $this->addMedia($image)->toMediaCollection('image');
        }
    }

    public function getImageAttribute()
    {
        return $this->getFirstMediaUrl('image');
    }

    public function getValidNumber()
    {
        return preg_replace('/[^\d]+/', '', $this->whatsapp);
    }

    public function getValidAgroNumber()
    {
        return preg_replace('/[^\d]+/', '', $this->agro_whatsapp);
    }
}
