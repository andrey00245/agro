<?php

namespace App\Http\Controllers;

use App\Faq;
use App\UserQuestion;
use Illuminate\Http\Request;

class FaqController extends Controller
{
    public function index()
    {
        return view('faq', [
            'faqs' => Faq::where('is_active', true)->orderBy('id', 'desc')->get(),
        ]);
    }

    public function question(Request $request)
    {
        if ($request->filled('question')) {

            UserQuestion::create($request->only('question'));

            session()->flash('question_sent', 'Ваш Вопрос успешно получен');

            return redirect()->back();
        }

        session()->flash('question_sent', 'Введите ваш вопрос!');

        return redirect()->back();
    }
}
