<?php

namespace App\Http\Controllers;

use App\Press;

class PressController extends Controller
{
    public function index()
    {
        return view('Presspage', [
            'presses' => Press::where('is_active', true)->orderBy('id', 'desc')->get()
        ]);
    }

    public function show()
    {
        return view('Presspage', [
            'presses' => Press::where('is_active', true)->orderBy('id', 'desc')->get()
        ]);
    }
}
