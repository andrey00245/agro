<?php

namespace App\Http\Controllers;

use App\CategoryVideo;
use App\CountryLand;
use App\Land;
use App\StaticPage;
use App\Video;
use Illuminate\Http\Request;

class LandsController extends Controller
{
    public function index()
    {
        $count = Land::where('is_active', true)->count();

        return view('land', [
            'lands' => Land::where('is_active', true)->limit(9)->orderBy('id', 'desc')->get(),
            'static_pages' => StaticPage::limit(2)->get(),
            'countries' => CountryLand::has('lands')->get(),
            'is_more' => $count > 9 ? true : false,
        ]);
    }

    public function allLends()
    {
        return view('land', [
            'lands' => Land::where('is_active', true)->orderBy('id', 'desc')->get(),
            'static_pages' => StaticPage::limit(2)->get(),
            'countries' => CountryLand::has('lands')->get(),
            'is_more' => false,
        ]);
    }


    public function show($id)
    {
        $land = Land::where('slug', $id)->firstOrFail();

        return view('productPage', [
            'land' => $land,
        ]);
    }

    public function sort(Request $request)
    {
        if (!$request->sort && $request->sort != 0) {
            abort('404');
        }

        if ($request->sort == 0) {
            return view('land', [
                'lands' => Land::where('is_active', true)->orderBy('id', 'desc')->get(),
                'static_pages' => StaticPage::limit(3)->get(),
                'countries' => CountryLand::has('lands')->get(),
            ]);
        }

        return view('land', [
            'lands' => CountryLand::where('id', $request->sort)->firstOrFail()->lands()->orderBy('id', 'desc')->get(),
            'static_pages' => StaticPage::limit(3)->get(),
            'countries' => CountryLand::has('lands')->get(),
        ]);
    }
}
