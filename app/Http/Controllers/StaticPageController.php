<?php

namespace App\Http\Controllers;

use App\StaticPage;
use App\Suggestion;

class StaticPageController extends Controller
{
    public function show($slug)
    {
        $static_page = StaticPage::where('slug', $slug)->firstOrFail();

        if ($static_page->id == 8) {
            return view('firstFlorPage', ['static_page' => $static_page]);
        }

        if ($static_page->id == 4 || $static_page->id == 7) {
            return view('static-page1', [
                'static_page' => $static_page,
            ]);
        }

        if ($static_page->id == 5 || $static_page->id == 6) {
            return view('static-page3', [
                'static_page' => $static_page,
            ]);
        }

        if ($static_page->id != 3) {
            return view('static-page', [
                'static_page' => $static_page,
            ]);
        }

        return view('buildOnLand', [
            'static_page' => $static_page,
            'winemark' => StaticPage::find(5),
            'illeon_system' => StaticPage::find(6),
        ]);
    }

    public function suggestion()
    {
        return view('suggestion',['suggestions' => Suggestion::all()]);
    }
}
