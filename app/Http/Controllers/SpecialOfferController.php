<?php

namespace App\Http\Controllers;

use App\CarPark;

class SpecialOfferController extends Controller
{
    public function index()
    {
        return view('special-offer', [
            'park_cars' => CarPark::all(),
        ]);
    }
}
