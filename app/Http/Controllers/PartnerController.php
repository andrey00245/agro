<?php

namespace App\Http\Controllers;

use App\Partner;

class PartnerController extends Controller
{
    public function index()
    {
        return view('partner',[
            'partners' => Partner::latest()->get(),
        ]);
    }
}
