<?php

namespace App\Http\Controllers;

use App\Article;
use App\ArticleCategory;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    public function index()
    {
        return view('articles',[
           'articles' => Article::latest()->get(),
            'categories' => ArticleCategory::whereHas('articles')->get(),
        ]);
    }

    public function sort(Request $request)
    {
        if ($request->filled('category')){
            return view('articles',[
                'articles' => Article::latest()->where('article_category_id', $request->category)->get(),
                'categories' => ArticleCategory::whereHas('articles')->get(),
            ]);
        }
        return view('articles',[
            'articles' => Article::latest()->get(),
            'categories' => ArticleCategory::whereHas('articles')->get(),
        ]);
    }
}
