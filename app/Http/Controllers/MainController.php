<?php

namespace App\Http\Controllers;

use App\Document;
use App\GoodToKnow;
use App\Press;
use App\StaticPage;
use App\Video;
use App\WeOffer;

class MainController extends Controller
{
    public function index()
    {
        return view('main', [
            'videos' => Video::where('is_active', true)->orderBy('id', 'desc')->limit(6)->get(),
            'weOffer' => WeOffer::all(),
        ]);
    }

    public function contact()
    {
        return view('ContactPage');
    }

    public function document()
    {
        return view('documents-page', [
            'documents' => Document::where('is_active', true)->orderBy('id', 'desc')->get()
        ]);
    }

    public function goodKnow()
    {
        return view('goodToKnowPage', [
            'goodKnows' => GoodToKnow::where('is_active', true)->orderBy('id', 'desc')->get()
        ]);
    }

    public function showGoodToKnow($slug)
    {
        return view('articlePage', [
            'goodKnow' => GoodToKnow::where('slug', $slug)->firstOrFail()
        ]);
    }

    public function winemaking()
    {
        return view('winemaking', [
            'static_page' => StaticPage::where('id', 4)->first(),
            'vino' => StaticPage::find(7),
            'first_step' => StaticPage::where('id',8)->first(),
        ]);
    }

    public function firstFlor()
    {
        $static_page = StaticPage::find(8);
        return view('firstFlorPage', ['static_page' => $static_page]);
    }

    public function locality()
    {
        return view('illionMapPage');
    }
}
