<?php

namespace App\Http\Controllers;

use App\Document;
use App\Feedback;
use App\Http\Requests\StoreRequest;

class FeedbackController extends Controller
{
    public function store(StoreRequest $request)
    {
        Feedback::create($request->only('name', 'email', 'comment', 'phone'));

        session()->flash('success', 'Ваша заявка Отправлена');

        $data = $request->all();

        if ($request->has('document')) {
            \Mail::send('mail', compact('data'), function ($message) use ($request) {
                $message->subject('Documents');
                $message->from('ilionInfo@gmail.com');
                $message->to($request['email']);

                foreach (\request('document') as $file) {
                    $message->attach(Document::find($file)->getFile());
                }
            });
        }

        return redirect()->back();

    }
}
