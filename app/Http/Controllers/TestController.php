<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Browsershot\Browsershot;



class TestController extends Controller
{
    public function screenshot(Request $request)
    {
        Browsershot::url('http://google.com')
            ->save("image.png");
        return 200;
    }
}
