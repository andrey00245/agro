<?php

namespace App\Http\Controllers;

use App\CategoryVideo;
use App\Video;
use Illuminate\Http\Request;

class VideoController extends Controller
{
    public function index()
    {
        $count = Video::where('is_active', true)->count();
        return view('video-page', [
            'videos' => Video::where('is_active', true)->limit(9)->orderBy('id', 'desc')->get(),
            'categories' => CategoryVideo::has('videos')->get(),
            'is_more' => $count > 9 ? true : false,
        ]);
    }

    public function allVideo()
    {
        return view('video-page', [
            'videos' => Video::where('is_active', true)->orderBy('id', 'desc')->get(),
            'categories' => CategoryVideo::has('videos')->get(),
            'is_more' => false,
        ]);
    }

    public function sort(Request $request)
    {
        if (!$request->sort_by && $request->sort_by != 0) {
            abort('404');
        }

        if ($request->sort_by == 0) {
            return view('video-page', [
                'videos' => Video::where('is_active', true)->limit(9)->orderBy('id', 'desc')->get(),
                'categories' => CategoryVideo::has('videos')->get(),
            ]);
        }

        return view('video-page', [
            'videos' => CategoryVideo::where('id', $request->sort_by)->firstOrFail()->videos()->where('is_active', true)->get(),
            'categories' => CategoryVideo::has('videos')->get(),
        ]);
    }
}
