<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Slider;
use App\WinemakerArticles;

class WinemakerController extends Controller
{
    public function index()
    {
        return view('winemaker',[
           'sliders' => Slider::query()->latest()->get(),
           'articles' => WinemakerArticles::query()->latest()->get(),
            'contact' => Contact::first(),
        ]);
    }
}
