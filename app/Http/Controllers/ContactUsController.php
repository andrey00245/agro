<?php

namespace App\Http\Controllers;

use App\ContactUs;
use App\Http\Requests\StoreContactUs;
use Illuminate\Http\Request;

class ContactUsController extends Controller
{
    public function store(StoreContactUs $request)
    {
        ContactUs::create($request->validated());

        return 200;
    }
}
