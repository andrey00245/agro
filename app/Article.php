<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Article extends Model implements HasMedia
{
    use InteractsWithMedia;

    protected $fillable = [
        'title',
    ];

    protected $dates = [
        'created_at',
    ];

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')
            ->width(216)
            ->height(200);
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('image')->singleFile();
        $this->addMediaCollection('file')->singleFile();
    }

    public function setImageAttribute($image)
    {
        $this->clearMediaCollection();

        if ($image !== null) {
            $this->addMedia($image)->toMediaCollection('image');
        }
    }
    public function setFileAttribute($file)
    {
        $this->clearMediaCollection();

        if ($file !== null) {
            $this->addMedia($file)->toMediaCollection('file');
        }
    }

    public function getfileAttribute($image)
    {
        if ($this->hasMedia('file') == null){
            return null;
        }

        return  $this->getFirstMedia('file')->getUrl();
    }

    public function getImageAttribute($image)
    {
        if ($this->hasMedia('image') == null){
            return asset('/img/partner.png');
        }

        return  $this->getFirstMedia('image')->getUrl();
    }

    public function articleCategory(): BelongsTo
    {
        return $this->belongsTo(ArticleCategory::class);
    }
}
