<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Date\Date;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Press extends Model implements HasMedia
{
    use InteractsWithMedia;

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb');
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('image')->singleFile();
    }

    public function setImageAttribute($image)
    {
        $this->clearMediaCollection();

        if ($image !== null) {
            $this->addMedia($image)->toMediaCollection('image');
        }
    }

    public function getCreatedAtAttribute($key)
    {
        return Date::parse($key)->format('j F Y года');
    }

    public function getShortDescription()
    {
        return strlen($this->description) > 140
            ?  mb_substr($this->description, 0, 140) . '...'
            : $this->description;
    }

    public function getMainImage()
    {
        return $this->getFirstMediaUrl('image') ?: null;
    }
}
