<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Jenssegers\Date\Date;


class GoodToKnow extends Model
{
    public static function boot()
    {
        parent::boot();

        static::saving(function ($model) {
            $model->slug = Str::slug($model->title);
        });
    }

    public function getCreatedAtAttribute($key)
    {
        return Date::parse($key)->format('j F Y года');
    }

    public function getShortDescription()
    {
        return strlen($this->description_block1) > 400
            ?  mb_substr($this->description_block1, 0, 400) . '...'
            : $this->description_block1;
    }
}
