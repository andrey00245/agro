<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Slider extends Model implements HasMedia
{
    use InteractsWithMedia;

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')
            ->width(216)
            ->height(200);
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('image')->singleFile();
    }

    public function setImageAttribute($image)
    {
        $this->clearMediaCollection();

        if ($image !== null) {
            $this->addMedia($image)->toMediaCollection('image');
        }
    }

    public function getImageAttribute(): ?string
    {
        return $this->getFirstMediaUrl('image') ?: null;
    }
}
