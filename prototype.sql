-- MySQL dump 10.13  Distrib 5.7.28, for Linux (x86_64)
--
-- Host: localhost    Database: prototype
-- ------------------------------------------------------
-- Server version	5.7.28-0ubuntu0.18.04.4

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `action_events`
--

DROP TABLE IF EXISTS `action_events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `action_events` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `batch_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `actionable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `actionable_id` bigint(20) unsigned NOT NULL,
  `target_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target_id` bigint(20) unsigned NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) unsigned DEFAULT NULL,
  `fields` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'running',
  `exception` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `original` text COLLATE utf8mb4_unicode_ci,
  `changes` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `action_events_actionable_type_actionable_id_index` (`actionable_type`,`actionable_id`),
  KEY `action_events_batch_id_model_type_model_id_index` (`batch_id`,`model_type`,`model_id`),
  KEY `action_events_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `action_events`
--

LOCK TABLES `action_events` WRITE;
/*!40000 ALTER TABLE `action_events` DISABLE KEYS */;
/*!40000 ALTER TABLE `action_events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contacts`
--

DROP TABLE IF EXISTS `contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contacts` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '#',
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `whatsapp` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '#',
  `instagram` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '#',
  `youtube` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '#',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacts`
--

LOCK TABLES `contacts` WRITE;
/*!40000 ALTER TABLE `contacts` DISABLE KEYS */;
INSERT INTO `contacts` VALUES (1,'868-697-7078','ydicki@yahoo.com','test address','#','#','#','2020-09-04 07:15:14','2020-09-04 07:15:14');
/*!40000 ALTER TABLE `contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `documents`
--

DROP TABLE IF EXISTS `documents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documents` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_request` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `documents`
--

LOCK TABLES `documents` WRITE;
/*!40000 ALTER TABLE `documents` DISABLE KEYS */;
INSERT INTO `documents` VALUES (1,'Sapiente quo quis a quos.','Et aspernatur distinctio fuga amet aut necessitatibus deserunt. Nam qui provident qui enim ut aut.',1,0,'2020-09-04 07:15:02','2020-09-04 07:15:02'),(2,'Quam aut earum qui provident sint et.','Voluptas ad sed nihil ratione nihil quaerat. Et aliquid nihil iure dicta.',1,0,'2020-09-04 07:15:02','2020-09-04 07:15:02'),(3,'Facere doloribus illo et.','Est doloribus officia necessitatibus in molestiae porro ut. Possimus nostrum excepturi tempore soluta ducimus. Non facere et sint odit.',1,0,'2020-09-04 07:15:02','2020-09-04 07:15:02'),(4,'Voluptatum aut dolorem et saepe quas voluptatibus.','Doloribus aliquam cupiditate consequatur reprehenderit perspiciatis qui quia repellendus. Nemo ut architecto numquam accusamus aut quo. Laborum repudiandae ipsam sunt laborum.',1,0,'2020-09-04 07:15:02','2020-09-04 07:15:02'),(5,'Quod et cupiditate aut unde illum accusantium quis.','Et dolore accusantium voluptatem dolorem assumenda et. Qui enim eos dolorem ut quam similique doloribus.',1,0,'2020-09-04 07:15:02','2020-09-04 07:15:02'),(6,'Facilis totam repudiandae magnam molestiae.','Distinctio iste eveniet aut. Soluta et pariatur inventore et. Id est sit blanditiis ut.',1,0,'2020-09-04 07:15:02','2020-09-04 07:15:02'),(7,'Est ducimus nesciunt veniam.','Quo nihil perferendis voluptatem suscipit ratione repellat atque. Nam esse dolores placeat dolores. Molestiae illum laudantium rerum.',1,0,'2020-09-04 07:15:02','2020-09-04 07:15:02'),(8,'Deserunt rerum nam in accusamus.','Recusandae ipsum culpa soluta qui sed facilis. Non asperiores sunt eum explicabo alias ratione. Et assumenda saepe ducimus soluta omnis ut.',1,0,'2020-09-04 07:15:02','2020-09-04 07:15:02'),(9,'Est quia quae autem adipisci repellendus quis.','Laudantium sapiente inventore deserunt corrupti minus voluptatibus qui. Doloribus eveniet vel accusantium quidem est. Et rem sunt rerum quia nobis sed voluptatibus nemo.',1,0,'2020-09-04 07:15:02','2020-09-04 07:15:02'),(10,'Voluptatem deserunt error nobis.','Aut doloribus dicta rem molestiae laborum iusto. Nobis iure ullam doloribus odio. Est velit dolorum voluptas quod id.',1,0,'2020-09-04 07:15:02','2020-09-04 07:15:02');
/*!40000 ALTER TABLE `documents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `faqs`
--

DROP TABLE IF EXISTS `faqs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `faqs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `question` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `answer` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `faqs`
--

LOCK TABLES `faqs` WRITE;
/*!40000 ALTER TABLE `faqs` DISABLE KEYS */;
INSERT INTO `faqs` VALUES (1,'Aut ipsam repudiandae velit.','Vel quasi illo quam officiis omnis qui quia. Libero officia quidem et maxime molestiae consequuntur nulla. Commodi quis omnis maxime ut velit sed ipsam.',1,'2020-09-04 07:15:02','2020-09-04 07:15:02'),(2,'Provident incidunt fugiat autem harum aut maiores.','Aspernatur rerum quae qui debitis itaque molestias. Optio ipsam ex veniam consequatur et. Voluptatibus cupiditate facere dicta modi velit. Temporibus et impedit repudiandae et voluptatem laborum.',1,'2020-09-04 07:15:02','2020-09-04 07:15:02'),(3,'Assumenda dolores consequuntur eum harum expedita omnis.','Molestias inventore et sunt. Ad iusto quaerat sapiente vitae. Nam ipsa a asperiores molestiae amet culpa. Sed fugit quam ut sit sapiente.',1,'2020-09-04 07:15:02','2020-09-04 07:15:02'),(4,'Quam temporibus molestias sed fuga nesciunt quidem.','Consequatur earum consequuntur quasi tenetur omnis. Enim omnis atque enim molestiae. Quidem ducimus dolorem magnam aut. Labore necessitatibus aut consequatur fugit vero alias eos.',1,'2020-09-04 07:15:02','2020-09-04 07:15:02'),(5,'Distinctio ratione minima error.','Est odit veritatis enim illo est aperiam aut. Reiciendis omnis nobis illo odio. Inventore ullam dolorem harum voluptatem. Dolorem voluptas error repellendus similique cumque.',1,'2020-09-04 07:15:02','2020-09-04 07:15:02'),(6,'At nulla temporibus quo.','Est rerum et ex consequuntur veniam accusantium voluptatem hic. Quaerat hic sint voluptatem perspiciatis eum similique. Sapiente non ducimus itaque numquam laudantium consectetur aut.',1,'2020-09-04 07:15:02','2020-09-04 07:15:02'),(7,'Ut fugit doloremque eos tempore voluptatem.','Vero porro iste provident aut magnam. In quod quas id. Quia aut sint molestiae quasi vel consequuntur dolores. Et culpa aliquam est amet debitis dolores.',1,'2020-09-04 07:15:02','2020-09-04 07:15:02'),(8,'Sed consequatur labore pariatur quasi velit molestias.','Quisquam deleniti natus consequuntur voluptatem. Ut ea eveniet eligendi ea dolores dolor. Aut quisquam dolor architecto aperiam. Eveniet maxime saepe at officiis est illo sint et.',1,'2020-09-04 07:15:02','2020-09-04 07:15:02'),(9,'Et voluptate voluptas sed ut.','Dolorem fugit omnis laboriosam voluptatum. Dolorem quasi necessitatibus ea possimus aut voluptates debitis. Aut non et rem et nobis voluptatibus sed. Maiores assumenda hic facilis quae.',1,'2020-09-04 07:15:02','2020-09-04 07:15:02'),(10,'Deserunt in commodi aut.','Consequatur sed et autem possimus sed assumenda incidunt. Est molestiae id velit praesentium. Et dolore dolorum dicta. Pariatur aut nesciunt exercitationem quo eos consequatur reprehenderit vel.',1,'2020-09-04 07:15:02','2020-09-04 07:15:02');
/*!40000 ALTER TABLE `faqs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `good_to_knows`
--

DROP TABLE IF EXISTS `good_to_knows`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `good_to_knows` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_block1` text COLLATE utf8mb4_unicode_ci,
  `description_block2` text COLLATE utf8mb4_unicode_ci,
  `description_block3` text COLLATE utf8mb4_unicode_ci,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `edition` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `good_to_knows`
--

LOCK TABLES `good_to_knows` WRITE;
/*!40000 ALTER TABLE `good_to_knows` DISABLE KEYS */;
INSERT INTO `good_to_knows` VALUES (1,'Casey O\'Keefe','Repellat molestias et odio fugit libero fugiat possimus. Excepturi ea assumenda et nobis. Quia iure explicabo voluptatem est saepe.','Eius et in facilis voluptatem pariatur. Tempore consequatur veritatis quisquam molestias tempore iusto.','Et cupiditate dignissimos qui consequatur. Rerum nihil quia qui qui architecto. Velit odit odit sint aperiam quia corrupti quisquam placeat.',1,'Selina Wolff','2020-09-04 07:15:02','2020-09-04 07:15:02'),(2,'Zion Purdy','Sequi dolore laboriosam quisquam quibusdam quasi voluptatem. Ab voluptatem quas ut aut ipsa. Magni est ea maiores inventore est.','Deserunt pariatur alias quis ducimus quia mollitia. Quaerat asperiores reiciendis iure et. Repellat incidunt est et sed et fugit non similique.','Eligendi sed aliquam magni quo aut. Quas ad maiores nemo unde qui. Corporis recusandae facilis eius blanditiis ut voluptatem laboriosam.',1,'Veda Luettgen','2020-09-04 07:15:02','2020-09-04 07:15:02'),(3,'Name Rice','Debitis officiis quo asperiores quo et. Quasi doloremque aspernatur quos fugiat. Voluptas eveniet odit quam veniam animi omnis recusandae dolorem. Ex eos itaque numquam ea id.','Laudantium vel deleniti est nulla. Autem quos itaque esse sed cumque et eligendi. Officiis qui sint quo dolor.','Error sed et qui corrupti id eligendi reiciendis. Veritatis id ipsam soluta. Repellat officiis quia quia sed. Qui corporis quia et ad nostrum ut voluptatem.',1,'Cecil Cronin','2020-09-04 07:15:02','2020-09-04 07:15:02'),(4,'Frida Daugherty PhD','Molestiae in autem sed. Asperiores sed molestiae sit at omnis in. Nulla qui eum sit delectus explicabo totam dolorum.','Eligendi aliquid at enim quidem eaque alias nesciunt. Asperiores est mollitia et quisquam atque fugiat. Eligendi tenetur quibusdam illum est esse. Rerum quo est optio fugiat quam quia impedit.','Est possimus id commodi et sint itaque. Ullam minus modi voluptas voluptas. Eaque sed dolore asperiores dolores.',1,'Florian McGlynn','2020-09-04 07:15:02','2020-09-04 07:15:02'),(5,'Hallie West DVM','Ut totam dolorem expedita ut in adipisci autem nihil. Officia maxime beatae maxime facilis. Dignissimos quos inventore hic cumque ea. Tempore est consequuntur error voluptatem et.','Rem quia voluptas qui alias ea ut. Aut error pariatur recusandae odit autem ratione. Qui amet molestiae ut voluptatem.','Et quo ullam architecto earum est. Asperiores aut aut velit nihil quo. Reiciendis ut a quo officiis.',1,'Meaghan Osinski','2020-09-04 07:15:02','2020-09-04 07:15:02'),(6,'Russell Herzog','Natus dolore voluptas repellat et. Voluptatem et ducimus est. Et soluta ipsam et omnis dicta nulla.','Voluptatem quasi laborum natus et iusto nobis. Aperiam assumenda qui mollitia autem dolores saepe.','Similique autem animi quaerat ullam omnis molestias vero. Velit ut et voluptate et magni ipsam eos. Sed non temporibus animi et ut. Dolor non ipsum atque mollitia nihil qui.',1,'Cory Boehm','2020-09-04 07:15:02','2020-09-04 07:15:02'),(7,'Laura Batz','Non eos accusamus consequatur quia. Molestiae qui laudantium fugiat laudantium. Et odio unde fuga soluta et magni. Ea placeat quae et earum. Qui voluptas ea totam rerum.','Nihil eius quae culpa ex commodi illo. Commodi aspernatur qui nesciunt ut cumque quas officia exercitationem. Optio nobis laboriosam saepe magni expedita. Quia quo quia ipsam quia.','Facilis eligendi ipsa voluptas a. Non ut fugit reiciendis sit laboriosam minus ipsam. Ad eos impedit aspernatur libero ut sapiente.',1,'Meredith Greenfelder','2020-09-04 07:15:02','2020-09-04 07:15:02'),(8,'Brock Kutch','Ipsum quis optio quam dicta. Vero aut est facilis sunt omnis provident animi. Neque iste dolor harum omnis voluptas expedita aut.','Perspiciatis ullam aspernatur perferendis qui consequatur error. Incidunt ab velit et harum qui optio est.','Sit repudiandae iste ipsum rerum repellendus fugit. Voluptatem maxime harum atque voluptate vero eligendi ea. Suscipit quia voluptatem facilis quis atque eveniet.',1,'Elmira Schimmel','2020-09-04 07:15:02','2020-09-04 07:15:02'),(9,'Theodora Waters','Similique sit maxime molestias sint. Ipsam officiis ipsam ut. Perspiciatis assumenda sed cumque ut nesciunt doloremque incidunt. Qui optio ut ut sequi quidem et. Ut culpa architecto nam non id.','Enim qui nostrum quia. Laudantium est corporis voluptatem minima. Enim tenetur delectus et porro aliquid dolorem enim.','Voluptatem aliquid perferendis quia tempora libero doloremque. Quas voluptatem assumenda autem accusamus ratione necessitatibus. Omnis in iste et voluptatum labore.',1,'Dimitri VonRueden','2020-09-04 07:15:02','2020-09-04 07:15:02'),(10,'Ethel Koelpin','Consequatur non laborum necessitatibus sit tempore dicta velit. Aut voluptatem omnis aperiam nesciunt aut suscipit accusantium quo. In nobis maiores voluptatem non sapiente voluptates est.','Quia ratione alias hic cumque deserunt qui. Itaque voluptate ut doloremque non cum. Porro repudiandae fugiat laborum non quod.','Tempora occaecati id dolorem quos est a. Sunt quisquam eaque iusto sed tenetur. Qui ullam ducimus impedit eos autem earum magni.',1,'Stacy Corkery','2020-09-04 07:15:02','2020-09-04 07:15:02');
/*!40000 ALTER TABLE `good_to_knows` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lands`
--

DROP TABLE IF EXISTS `lands`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lands` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `area` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cost` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cost_for_object` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `map` text COLLATE utf8mb4_unicode_ci,
  `video_url` text COLLATE utf8mb4_unicode_ci,
  `video_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `video_description` text COLLATE utf8mb4_unicode_ci,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lands`
--

LOCK TABLES `lands` WRITE;
/*!40000 ALTER TABLE `lands` DISABLE KEYS */;
INSERT INTO `lands` VALUES (1,'Inventore dolor alias consequatur.','Quis consequatur consequuntur praesentium quod amet voluptatem. Est eveniet rerum assumenda odio esse facilis ipsum accusamus. Consequatur dolor quibusdam fuga nihil.','125','600','500','<iframe width=\"520\" height=\"400\" frameborder=\"0\" scrolling=\"no\" marginheight=\"0\" marginwidth=\"0\" id=\"gmap_canvas\" src=\"https://maps.google.com/maps?width=520&amp;height=400&amp;hl=en&amp;q=%20Kharkiv+()&amp;t=&amp;z=12&amp;ie=UTF8&amp;iwloc=B&amp;output=embed\"></iframe> <a href=\'http://maps-generator.com/ru\'>Maps-Generator.com/ru</a> <script type=\'text/javascript\' src=\'https://maps-generator.com/google-maps-authorization/script.js?id=4d57ad94ca2459087a602b70bdaa8312ae0cd186\'></script>','http://www.youtube.com/embed/KWjqicp6Lt0','Animi error quae qui rerum voluptatem labore.','Nemo vel non et reiciendis ipsam quia. Neque autem nemo vero. Accusantium dolor magnam ut fugit iusto quasi.',1,'2020-09-04 07:15:06','2020-09-04 07:15:06'),(2,'Atque hic animi minima facilis culpa repellendus.','Expedita omnis ea qui tempore dolores cupiditate. Aut id id ea ipsam provident esse est. Ad et dolorem deserunt rerum corrupti ullam. Omnis quaerat non et animi voluptas sed. Sapiente ea est enim.','125','600','500','<iframe width=\"520\" height=\"400\" frameborder=\"0\" scrolling=\"no\" marginheight=\"0\" marginwidth=\"0\" id=\"gmap_canvas\" src=\"https://maps.google.com/maps?width=520&amp;height=400&amp;hl=en&amp;q=%20Kharkiv+()&amp;t=&amp;z=12&amp;ie=UTF8&amp;iwloc=B&amp;output=embed\"></iframe> <a href=\'http://maps-generator.com/ru\'>Maps-Generator.com/ru</a> <script type=\'text/javascript\' src=\'https://maps-generator.com/google-maps-authorization/script.js?id=4d57ad94ca2459087a602b70bdaa8312ae0cd186\'></script>','http://www.youtube.com/embed/KWjqicp6Lt0','Itaque ut voluptatem est quibusdam excepturi ut.','Perferendis magni architecto animi velit et maxime. Fuga quia reprehenderit eos ut accusantium harum sed. Quia repudiandae in quisquam unde qui.',1,'2020-09-04 07:15:08','2020-09-04 07:15:08'),(3,'Voluptas necessitatibus vel qui qui.','Tenetur reiciendis eos assumenda velit voluptatem. Rerum est animi adipisci voluptate id labore suscipit dolore. Sapiente accusamus sit et tempore est vero. Quia alias praesentium et rerum ut.','125','600','500','<iframe width=\"520\" height=\"400\" frameborder=\"0\" scrolling=\"no\" marginheight=\"0\" marginwidth=\"0\" id=\"gmap_canvas\" src=\"https://maps.google.com/maps?width=520&amp;height=400&amp;hl=en&amp;q=%20Kharkiv+()&amp;t=&amp;z=12&amp;ie=UTF8&amp;iwloc=B&amp;output=embed\"></iframe> <a href=\'http://maps-generator.com/ru\'>Maps-Generator.com/ru</a> <script type=\'text/javascript\' src=\'https://maps-generator.com/google-maps-authorization/script.js?id=4d57ad94ca2459087a602b70bdaa8312ae0cd186\'></script>','http://www.youtube.com/embed/KWjqicp6Lt0','Fuga dolor labore et sed quia.','Voluptatem ut illo mollitia fugiat placeat autem. Voluptate doloremque vitae voluptas quis quo.',1,'2020-09-04 07:15:08','2020-09-04 07:15:08'),(4,'Voluptatem a magni sunt asperiores consequatur velit.','Et omnis est omnis maiores. Cum nulla accusamus aut sint perferendis quo eveniet. Temporibus qui facere vero. Eos eveniet modi magni consequuntur temporibus et.','125','600','500','<iframe width=\"520\" height=\"400\" frameborder=\"0\" scrolling=\"no\" marginheight=\"0\" marginwidth=\"0\" id=\"gmap_canvas\" src=\"https://maps.google.com/maps?width=520&amp;height=400&amp;hl=en&amp;q=%20Kharkiv+()&amp;t=&amp;z=12&amp;ie=UTF8&amp;iwloc=B&amp;output=embed\"></iframe> <a href=\'http://maps-generator.com/ru\'>Maps-Generator.com/ru</a> <script type=\'text/javascript\' src=\'https://maps-generator.com/google-maps-authorization/script.js?id=4d57ad94ca2459087a602b70bdaa8312ae0cd186\'></script>','http://www.youtube.com/embed/KWjqicp6Lt0','Enim officiis dolorem facilis nihil.','Et alias quos eius dolorum. Culpa qui non aut reprehenderit quia qui. Recusandae quaerat voluptatem rem accusantium et. At ex molestias animi autem.',1,'2020-09-04 07:15:08','2020-09-04 07:15:08'),(5,'Magni facere aliquam ut dolorem nihil.','Laudantium quas alias voluptas animi. Sit excepturi blanditiis dicta harum. Rem est doloribus modi eum quidem ut.','125','600','500','<iframe width=\"520\" height=\"400\" frameborder=\"0\" scrolling=\"no\" marginheight=\"0\" marginwidth=\"0\" id=\"gmap_canvas\" src=\"https://maps.google.com/maps?width=520&amp;height=400&amp;hl=en&amp;q=%20Kharkiv+()&amp;t=&amp;z=12&amp;ie=UTF8&amp;iwloc=B&amp;output=embed\"></iframe> <a href=\'http://maps-generator.com/ru\'>Maps-Generator.com/ru</a> <script type=\'text/javascript\' src=\'https://maps-generator.com/google-maps-authorization/script.js?id=4d57ad94ca2459087a602b70bdaa8312ae0cd186\'></script>','http://www.youtube.com/embed/KWjqicp6Lt0','Ex eos possimus nihil iusto.','Consequatur quia tempore deserunt. Officiis voluptatem pariatur enim asperiores. Error enim harum nam qui voluptatem. Quam dignissimos incidunt quia aut.',1,'2020-09-04 07:15:08','2020-09-04 07:15:08'),(6,'Quasi cupiditate aut qui quaerat.','Adipisci repellendus necessitatibus delectus qui dolorem. Unde dolor porro similique sequi excepturi sint. Magni ipsum consequatur quia qui dolor et reprehenderit. Quisquam iure et aut.','125','600','500','<iframe width=\"520\" height=\"400\" frameborder=\"0\" scrolling=\"no\" marginheight=\"0\" marginwidth=\"0\" id=\"gmap_canvas\" src=\"https://maps.google.com/maps?width=520&amp;height=400&amp;hl=en&amp;q=%20Kharkiv+()&amp;t=&amp;z=12&amp;ie=UTF8&amp;iwloc=B&amp;output=embed\"></iframe> <a href=\'http://maps-generator.com/ru\'>Maps-Generator.com/ru</a> <script type=\'text/javascript\' src=\'https://maps-generator.com/google-maps-authorization/script.js?id=4d57ad94ca2459087a602b70bdaa8312ae0cd186\'></script>','http://www.youtube.com/embed/KWjqicp6Lt0','Enim hic quae corrupti quo harum.','Aut amet minus veritatis molestiae sit. Sed illum voluptas ea odio minus pariatur. Ipsam consequatur numquam illum quis voluptates.',1,'2020-09-04 07:15:09','2020-09-04 07:15:09'),(7,'Deserunt qui aut velit labore deleniti.','Placeat voluptatibus aliquid sint et consequuntur quidem est. Atque ullam aut modi aliquid voluptatum sunt. Quia vel at veritatis consequatur eveniet in voluptatem.','125','600','500','<iframe width=\"520\" height=\"400\" frameborder=\"0\" scrolling=\"no\" marginheight=\"0\" marginwidth=\"0\" id=\"gmap_canvas\" src=\"https://maps.google.com/maps?width=520&amp;height=400&amp;hl=en&amp;q=%20Kharkiv+()&amp;t=&amp;z=12&amp;ie=UTF8&amp;iwloc=B&amp;output=embed\"></iframe> <a href=\'http://maps-generator.com/ru\'>Maps-Generator.com/ru</a> <script type=\'text/javascript\' src=\'https://maps-generator.com/google-maps-authorization/script.js?id=4d57ad94ca2459087a602b70bdaa8312ae0cd186\'></script>','http://www.youtube.com/embed/KWjqicp6Lt0','Reiciendis laborum voluptatem id vero sint.','Fugit et quia exercitationem quae. Quo unde est quis. Ad pariatur voluptatem dolorum vel cumque.',1,'2020-09-04 07:15:09','2020-09-04 07:15:09'),(8,'Aut sit unde quibusdam.','Qui enim quaerat aperiam nulla earum. Est necessitatibus minus modi vel. Dicta in ea consequatur porro.','125','600','500','<iframe width=\"520\" height=\"400\" frameborder=\"0\" scrolling=\"no\" marginheight=\"0\" marginwidth=\"0\" id=\"gmap_canvas\" src=\"https://maps.google.com/maps?width=520&amp;height=400&amp;hl=en&amp;q=%20Kharkiv+()&amp;t=&amp;z=12&amp;ie=UTF8&amp;iwloc=B&amp;output=embed\"></iframe> <a href=\'http://maps-generator.com/ru\'>Maps-Generator.com/ru</a> <script type=\'text/javascript\' src=\'https://maps-generator.com/google-maps-authorization/script.js?id=4d57ad94ca2459087a602b70bdaa8312ae0cd186\'></script>','http://www.youtube.com/embed/KWjqicp6Lt0','Quos et harum rerum ipsa.','Iste vitae necessitatibus nemo nulla dolores fugiat quo. Delectus et natus quasi. Enim a sequi et ea. Recusandae soluta omnis possimus voluptate omnis.',1,'2020-09-04 07:15:09','2020-09-04 07:15:09'),(9,'Non eius fugit neque necessitatibus eius.','Illo eos ut consequatur necessitatibus. Fugiat aut voluptatem repellendus aut non doloribus tenetur. Iure at id quas sunt.','125','600','500','<iframe width=\"520\" height=\"400\" frameborder=\"0\" scrolling=\"no\" marginheight=\"0\" marginwidth=\"0\" id=\"gmap_canvas\" src=\"https://maps.google.com/maps?width=520&amp;height=400&amp;hl=en&amp;q=%20Kharkiv+()&amp;t=&amp;z=12&amp;ie=UTF8&amp;iwloc=B&amp;output=embed\"></iframe> <a href=\'http://maps-generator.com/ru\'>Maps-Generator.com/ru</a> <script type=\'text/javascript\' src=\'https://maps-generator.com/google-maps-authorization/script.js?id=4d57ad94ca2459087a602b70bdaa8312ae0cd186\'></script>','http://www.youtube.com/embed/KWjqicp6Lt0','Reprehenderit unde modi quo quia repellendus hic.','Nihil beatae amet possimus fugiat. Sunt aperiam dolor voluptatem consequatur fugit tenetur culpa. Et voluptas at quasi.',1,'2020-09-04 07:15:09','2020-09-04 07:15:09'),(10,'Expedita vitae dolorum sint maxime tempora aperiam.','Possimus dolores illum id. Enim similique omnis soluta suscipit nemo voluptatum. Perspiciatis sunt aut aut consequatur. Nostrum fugit impedit molestiae libero assumenda molestias.','125','600','500','<iframe width=\"520\" height=\"400\" frameborder=\"0\" scrolling=\"no\" marginheight=\"0\" marginwidth=\"0\" id=\"gmap_canvas\" src=\"https://maps.google.com/maps?width=520&amp;height=400&amp;hl=en&amp;q=%20Kharkiv+()&amp;t=&amp;z=12&amp;ie=UTF8&amp;iwloc=B&amp;output=embed\"></iframe> <a href=\'http://maps-generator.com/ru\'>Maps-Generator.com/ru</a> <script type=\'text/javascript\' src=\'https://maps-generator.com/google-maps-authorization/script.js?id=4d57ad94ca2459087a602b70bdaa8312ae0cd186\'></script>','http://www.youtube.com/embed/KWjqicp6Lt0','Molestiae et alias aliquid vitae.','Dicta consequatur ullam et accusamus unde. Earum et ea distinctio rerum. Non quibusdam voluptates perferendis corrupti consequatur.',1,'2020-09-04 07:15:09','2020-09-04 07:15:09');
/*!40000 ALTER TABLE `lands` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media`
--

DROP TABLE IF EXISTS `media`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  `uuid` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `collection_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mime_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `disk` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `conversions_disk` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size` bigint(20) unsigned NOT NULL,
  `manipulations` json NOT NULL,
  `custom_properties` json NOT NULL,
  `responsive_images` json NOT NULL,
  `order_column` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `media_model_type_model_id_index` (`model_type`,`model_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media`
--

LOCK TABLES `media` WRITE;
/*!40000 ALTER TABLE `media` DISABLE KEYS */;
INSERT INTO `media` VALUES (1,'App\\Land',1,'a773ff41-6b1c-41fc-bd52-b827ed32a4ad','image','0e16a3ff1407144d2099b40dd762081d','0e16a3ff1407144d2099b40dd762081d.jpg','image/jpeg','public','public',31444,'[]','{\"generated_conversions\": {\"thumb\": true}}','[]',1,'2020-09-04 07:15:06','2020-09-04 07:15:08'),(2,'App\\Land',2,'c8d5ce51-2f66-42eb-a570-a46bf584c59a','image','9ff30e9ea91dedcc50773633934ae567','9ff30e9ea91dedcc50773633934ae567.jpg','image/jpeg','public','public',54791,'[]','{\"generated_conversions\": {\"thumb\": true}}','[]',2,'2020-09-04 07:15:08','2020-09-04 07:15:08'),(3,'App\\Land',3,'5bc2aaf3-6591-4c47-8e51-3e12b9726f81','image','76f1dbbdd5da11d2bf64a2c74c39288b','76f1dbbdd5da11d2bf64a2c74c39288b.jpg','image/jpeg','public','public',22440,'[]','{\"generated_conversions\": {\"thumb\": true}}','[]',3,'2020-09-04 07:15:08','2020-09-04 07:15:08'),(4,'App\\Land',4,'2aaa32ce-be02-4962-9540-8a0912580d17','image','c9067900723dd35748e947ba48355bf2','c9067900723dd35748e947ba48355bf2.jpg','image/jpeg','public','public',83737,'[]','{\"generated_conversions\": {\"thumb\": true}}','[]',4,'2020-09-04 07:15:08','2020-09-04 07:15:08'),(5,'App\\Land',5,'a3f246a8-05c3-4b87-8460-97f93761e33a','image','d6c53f3fb26d812b226232cb889e5957','d6c53f3fb26d812b226232cb889e5957.jpg','image/jpeg','public','public',41700,'[]','{\"generated_conversions\": {\"thumb\": true}}','[]',5,'2020-09-04 07:15:08','2020-09-04 07:15:09'),(6,'App\\Land',6,'2d02c5dc-8803-4892-893c-01fdb580393c','image','5bb8f3f4430a0dce4c931649ab68bb61','5bb8f3f4430a0dce4c931649ab68bb61.jpg','image/jpeg','public','public',30574,'[]','{\"generated_conversions\": {\"thumb\": true}}','[]',6,'2020-09-04 07:15:09','2020-09-04 07:15:09'),(7,'App\\Land',7,'2df9d95d-2c53-4c2c-b42d-b384e56aab4b','image','346ee1a57b0af30e8e78565b64f2dcfc','346ee1a57b0af30e8e78565b64f2dcfc.jpg','image/jpeg','public','public',42308,'[]','{\"generated_conversions\": {\"thumb\": true}}','[]',7,'2020-09-04 07:15:09','2020-09-04 07:15:09'),(8,'App\\Land',8,'99b1ccf7-4ef6-4086-8476-10e9e5fbe33a','image','0a0235ccb51f81756012abc44a592f23','0a0235ccb51f81756012abc44a592f23.jpg','image/jpeg','public','public',73389,'[]','{\"generated_conversions\": {\"thumb\": true}}','[]',8,'2020-09-04 07:15:09','2020-09-04 07:15:09'),(9,'App\\Land',9,'0f49a149-5ece-4050-aef8-e54692f6259e','image','56dee1a263dc94896252543fb1f8db0b','56dee1a263dc94896252543fb1f8db0b.jpg','image/jpeg','public','public',51931,'[]','{\"generated_conversions\": {\"thumb\": true}}','[]',9,'2020-09-04 07:15:09','2020-09-04 07:15:09'),(10,'App\\Land',10,'94efba46-84ef-4d68-8a1a-b4c148de2ac8','image','973061f281ef1286a2e2f324d0be6292','973061f281ef1286a2e2f324d0be6292.jpg','image/jpeg','public','public',29583,'[]','{\"generated_conversions\": {\"thumb\": true}}','[]',10,'2020-09-04 07:15:09','2020-09-04 07:15:09'),(11,'App\\Press',1,'55b6275a-78b2-41d6-9c72-27c2926156c1','image','25bbc2a4b7b12eb9835f8fd2d709ef55','25bbc2a4b7b12eb9835f8fd2d709ef55.jpg','image/jpeg','public','public',68796,'[]','{\"generated_conversions\": {\"thumb\": true}}','[]',11,'2020-09-04 07:15:13','2020-09-04 07:15:13'),(12,'App\\Press',2,'005a2ff4-5793-4471-8afc-abbe917c34b0','image','34e86f417149b3af29b10556fb99f3e9','34e86f417149b3af29b10556fb99f3e9.jpg','image/jpeg','public','public',50084,'[]','{\"generated_conversions\": {\"thumb\": true}}','[]',12,'2020-09-04 07:15:13','2020-09-04 07:15:13'),(13,'App\\Press',3,'c7ac19f2-c0e4-4250-afc3-a7ce047e4bbb','image','49cf3b3fbeae9acce18ea8ba7b4b75f2','49cf3b3fbeae9acce18ea8ba7b4b75f2.jpg','image/jpeg','public','public',36290,'[]','{\"generated_conversions\": {\"thumb\": true}}','[]',13,'2020-09-04 07:15:13','2020-09-04 07:15:13'),(14,'App\\Press',4,'802481aa-d3a6-4b47-80cb-dc84ca8afa22','image','6cf0da46bfc54459660bc877a201fad8','6cf0da46bfc54459660bc877a201fad8.jpg','image/jpeg','public','public',59684,'[]','{\"generated_conversions\": {\"thumb\": true}}','[]',14,'2020-09-04 07:15:13','2020-09-04 07:15:13'),(15,'App\\Press',5,'5b05c8d0-4f51-453f-b0d3-47a20ebb110e','image','b39476d6cb54fb6128ebadef9ab1f1af','b39476d6cb54fb6128ebadef9ab1f1af.jpg','image/jpeg','public','public',57206,'[]','{\"generated_conversions\": {\"thumb\": true}}','[]',15,'2020-09-04 07:15:13','2020-09-04 07:15:13'),(16,'App\\Press',6,'b9db8a92-fbde-4f62-a647-4ae3e93a36ef','image','fc8c8914f516d21f3f1ab97f5d54cbb6','fc8c8914f516d21f3f1ab97f5d54cbb6.jpg','image/jpeg','public','public',37488,'[]','{\"generated_conversions\": {\"thumb\": true}}','[]',16,'2020-09-04 07:15:13','2020-09-04 07:15:13'),(17,'App\\Press',7,'b7c0adbf-f30c-4810-9456-4a3f19f2a793','image','5450e2ebd3e99ebfa42189da098cc919','5450e2ebd3e99ebfa42189da098cc919.jpg','image/jpeg','public','public',38315,'[]','{\"generated_conversions\": {\"thumb\": true}}','[]',17,'2020-09-04 07:15:13','2020-09-04 07:15:14'),(18,'App\\Press',8,'85d43f2c-74f9-448c-ae7b-7034737937e4','image','0d28c29536d45eaf948cb4c939e65ebe','0d28c29536d45eaf948cb4c939e65ebe.jpg','image/jpeg','public','public',31252,'[]','{\"generated_conversions\": {\"thumb\": true}}','[]',18,'2020-09-04 07:15:14','2020-09-04 07:15:14'),(19,'App\\Press',9,'53d45e97-62b7-478f-b83a-b128cdb38011','image','5cb5e8d90a9ccc5db912cdf359d34e69','5cb5e8d90a9ccc5db912cdf359d34e69.jpg','image/jpeg','public','public',68978,'[]','{\"generated_conversions\": {\"thumb\": true}}','[]',19,'2020-09-04 07:15:14','2020-09-04 07:15:14'),(20,'App\\Press',10,'2bf10a35-d24f-42b1-8a1d-5135fb37e3d0','image','d7a1e7f22f020fb6903942f1d65aa4e4','d7a1e7f22f020fb6903942f1d65aa4e4.jpg','image/jpeg','public','public',5491,'[]','{\"generated_conversions\": {\"thumb\": true}}','[]',20,'2020-09-04 07:15:14','2020-09-04 07:15:14');
/*!40000 ALTER TABLE `media` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2018_01_01_000000_create_action_events_table',1),(4,'2019_05_10_000000_add_fields_to_action_events_table',1),(5,'2019_08_19_000000_create_failed_jobs_table',1),(6,'2020_08_09_183047_create_media_table',1),(7,'2020_09_03_181023_create_videos_table',1),(8,'2020_09_03_181153_create_contacts_table',1),(9,'2020_09_03_181307_create_presses_table',1),(10,'2020_09_03_181434_create_faqs_table',1),(11,'2020_09_03_181523_create_good_to_knows_table',1),(12,'2020_09_03_181634_create_user_questions_table',1),(13,'2020_09_03_181725_create_documents_table',1),(14,'2020_09_03_181807_create_lands_table',1),(15,'2020_09_03_181903_create_we_offers_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `presses`
--

DROP TABLE IF EXISTS `presses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `presses` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `presses`
--

LOCK TABLES `presses` WRITE;
/*!40000 ALTER TABLE `presses` DISABLE KEYS */;
INSERT INTO `presses` VALUES (1,'Et ut et quibusdam minus reiciendis recusandae.','Eligendi numquam ea occaecati cupiditate at nihil cum. Eius reprehenderit quos velit animi asperiores placeat dolorum voluptas. Consequatur rerum aspernatur qui corporis quia.','http://hansen.biz/nemo-reiciendis-expedita-pariatur-quibusdam-temporibus',1,'2020-09-04 07:15:13','2020-09-04 07:15:13'),(2,'Eaque molestiae maiores quis reiciendis odio minus.','Laborum natus voluptatibus totam tenetur autem et. Sed repellat qui ullam ex. Labore consequatur id et corrupti aliquam qui ducimus. Eveniet unde cum quia aliquid.','http://olson.com/sed-odio-sit-repellendus-quod.html',1,'2020-09-04 07:15:13','2020-09-04 07:15:13'),(3,'Neque mollitia velit voluptatem voluptatum sit voluptatem.','Consequatur aut cupiditate rerum nobis porro enim voluptate. Autem sit consequatur dignissimos esse ut. Mollitia facilis ipsa corporis labore dolor in corrupti.','https://www.ernser.com/quam-id-qui-iusto-id-aut-repellat',1,'2020-09-04 07:15:13','2020-09-04 07:15:13'),(4,'Qui qui ut consectetur vitae dignissimos.','Eius at repellat qui voluptatem dolores optio et autem. Commodi ut molestias in veniam temporibus at. Error vitae fugiat id quam rerum. Rerum expedita praesentium itaque sunt sit minima consequatur.','https://www.turcotte.net/eos-cumque-ipsum-provident-quasi-consectetur-eos-at',1,'2020-09-04 07:15:13','2020-09-04 07:15:13'),(5,'Asperiores alias reiciendis laboriosam.','Quo cum et quia sed omnis earum. Adipisci at accusamus maxime in qui voluptates quia. Quos perspiciatis architecto molestiae voluptatem omnis ea libero.','http://www.heller.com/ad-id-deleniti-facere-tempore-deleniti-enim-et.html',1,'2020-09-04 07:15:13','2020-09-04 07:15:13'),(6,'Voluptatem accusamus quo rerum ea.','Cumque et pariatur sed totam non earum deserunt. Occaecati et id velit optio voluptatem. Culpa et modi ut corrupti sequi sed cupiditate. Dignissimos quis autem qui impedit error asperiores soluta.','http://johns.biz/qui-nulla-asperiores-ad-placeat',1,'2020-09-04 07:15:13','2020-09-04 07:15:13'),(7,'Accusantium id explicabo asperiores.','Et aspernatur non ut minima. Consequatur ut quo ut. Saepe iste temporibus aperiam ut quod ut quo excepturi. Repudiandae fugiat ut ab.','http://www.brekke.info/perferendis-tempora-molestiae-totam-consequatur-quia-vitae',1,'2020-09-04 07:15:13','2020-09-04 07:15:13'),(8,'Inventore at est ut accusamus quo incidunt.','Dolorem nobis magni mollitia qui suscipit quidem quod. Quasi eveniet dolorem est qui error quis. Explicabo ut eum vel dolorem molestias rerum. Laudantium voluptatem vel est nihil.','http://www.runolfsdottir.org/aut-culpa-aut-vitae-quia',1,'2020-09-04 07:15:14','2020-09-04 07:15:14'),(9,'Dolores ipsum at aut corrupti ad.','Inventore voluptatem ad ut velit. Soluta sed maxime nam deserunt porro labore voluptatem. Omnis cum eligendi omnis blanditiis minus rem.','http://schultz.org/',1,'2020-09-04 07:15:14','2020-09-04 07:15:14'),(10,'Iusto animi facilis delectus non quisquam voluptatem.','Veniam ipsa veritatis qui in dicta et totam. Et sit similique numquam vel. Quod quo fuga sint omnis temporibus et.','http://www.murphy.biz/vero-velit-asperiores-consequatur-facere-soluta',1,'2020-09-04 07:15:14','2020-09-04 07:15:14');
/*!40000 ALTER TABLE `presses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_questions`
--

DROP TABLE IF EXISTS `user_questions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_questions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `question` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_questions`
--

LOCK TABLES `user_questions` WRITE;
/*!40000 ALTER TABLE `user_questions` DISABLE KEYS */;
INSERT INTO `user_questions` VALUES (1,'Nam est dolore ab aut suscipit. Quos est dignissimos autem ad ea dolore. Magni ullam corporis sit. Vero est aut alias in dolore id amet id.','просмотрен','2020-09-04 07:15:14','2020-09-04 07:15:14'),(2,'Sed temporibus est aut omnis dolores et. Neque dicta ut non dolorum ea. Qui autem expedita quisquam excepturi.','новый','2020-09-04 07:15:14','2020-09-04 07:15:14'),(3,'Ut nemo doloribus qui impedit sapiente ab qui repudiandae. Cum ullam aliquid ipsa eaque. Et quis et quia asperiores rerum.','новый','2020-09-04 07:15:14','2020-09-04 07:15:14'),(4,'Quia necessitatibus voluptas iure alias in aut. Quis ipsa deleniti distinctio veniam. Velit nihil voluptatem aut a labore illum.','новый','2020-09-04 07:15:14','2020-09-04 07:15:14'),(5,'Minima voluptatem eos quia sed aliquid aliquam. Magnam sed quisquam assumenda omnis enim. Consectetur voluptatem omnis aut beatae laboriosam non.','новый','2020-09-04 07:15:14','2020-09-04 07:15:14'),(6,'Nulla at suscipit consequatur repellat. Ea voluptatem doloribus repellendus rem. Incidunt non saepe iusto. Qui nemo aut quam ea.','просмотрен','2020-09-04 07:15:14','2020-09-04 07:15:14'),(7,'Et autem atque illo. Sit sed quis exercitationem officiis. Exercitationem non aperiam modi rerum nulla quo. Sequi magnam atque ea doloribus vel.','просмотрен','2020-09-04 07:15:14','2020-09-04 07:15:14'),(8,'Possimus ut nemo sapiente assumenda. Velit deleniti doloremque ad enim corrupti ducimus similique. Natus quaerat quia eos eos.','просмотрен','2020-09-04 07:15:14','2020-09-04 07:15:14'),(9,'Eius ea ab numquam quae et. Eligendi excepturi tempora nostrum unde quasi. Doloribus ducimus velit dolores doloremque quia quia esse.','просмотрен','2020-09-04 07:15:14','2020-09-04 07:15:14'),(10,'Fuga non tenetur hic. Architecto id dicta id autem. Reiciendis repudiandae adipisci iusto est autem.','новый','2020-09-04 07:15:14','2020-09-04 07:15:14');
/*!40000 ALTER TABLE `user_questions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Account','admin@example.com',NULL,'$2y$10$xhJ/qWzslDQYqCOnkmnySuII4p1iFKbM2NLLrVIpIBBE9QvNUCv4a',NULL,'2020-09-04 07:15:14','2020-09-04 07:15:14');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `videos`
--

DROP TABLE IF EXISTS `videos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `videos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `videos`
--

LOCK TABLES `videos` WRITE;
/*!40000 ALTER TABLE `videos` DISABLE KEYS */;
INSERT INTO `videos` VALUES (1,'Modi facere debitis fugiat in exercitationem eligendi.','Aliquid velit aut nobis et sunt aut. Sit aut deleniti consequatur ex. Excepturi voluptatem consequatur corporis beatae sit architecto. Veniam cum fugit magnam sapiente fugiat cumque explicabo sed.','http://www.youtube.com/embed/KWjqicp6Lt0',1,'2020-09-04 07:15:14','2020-09-04 07:15:14'),(2,'Facere autem quaerat assumenda.','Quaerat nihil sint non aut qui deserunt amet. Voluptas reprehenderit et dolorem ut odio laborum. Eum adipisci non quae consectetur. Culpa itaque eveniet ut.','http://www.youtube.com/embed/KWjqicp6Lt0',1,'2020-09-04 07:15:14','2020-09-04 07:15:14'),(3,'Sit esse aut omnis.','Rerum consequatur asperiores aut nesciunt. Cumque ut sed fugit. Rerum molestias saepe facere culpa.','http://www.youtube.com/embed/KWjqicp6Lt0',1,'2020-09-04 07:15:14','2020-09-04 07:15:14'),(4,'Ipsum mollitia qui explicabo ut.','Labore velit quaerat ut earum. Facilis quia autem nostrum ex voluptatem. Minima praesentium nostrum distinctio qui sunt ut. Architecto maiores velit occaecati quisquam similique saepe.','http://www.youtube.com/embed/KWjqicp6Lt0',1,'2020-09-04 07:15:14','2020-09-04 07:15:14'),(5,'Alias ipsum nemo omnis odit aliquid rem.','Sapiente aut consequatur iusto nihil consequatur non. Accusamus suscipit aut sit nihil aliquid voluptatibus. Quibusdam harum voluptas est at pariatur voluptas.','http://www.youtube.com/embed/KWjqicp6Lt0',1,'2020-09-04 07:15:14','2020-09-04 07:15:14'),(6,'Nostrum non quas consequatur.','Mollitia quam repellat ullam odio nesciunt. Odio velit sint fuga culpa tempora qui. Aut id aperiam aut iure repellat error cum ut.','http://www.youtube.com/embed/KWjqicp6Lt0',1,'2020-09-04 07:15:14','2020-09-04 07:15:14'),(7,'Neque molestiae voluptas libero.','Fugiat repellendus ipsa aperiam et labore vel. Consequatur sunt sunt placeat iste repudiandae. Minima exercitationem harum dolores odio aliquam dolorum. Aut eum aperiam minima praesentium.','http://www.youtube.com/embed/KWjqicp6Lt0',1,'2020-09-04 07:15:14','2020-09-04 07:15:14'),(8,'Et alias omnis deserunt.','Neque doloribus quo nobis est sint ducimus. Quia eius ratione porro odio. Recusandae in voluptas et repellat nesciunt id. Deserunt omnis ut maxime omnis temporibus qui.','http://www.youtube.com/embed/KWjqicp6Lt0',1,'2020-09-04 07:15:14','2020-09-04 07:15:14'),(9,'Non voluptatem reprehenderit aut.','Sapiente nemo natus vel ea illo. Temporibus vero eos rem dolorem rerum laborum sit tenetur. Dolorem perferendis sed omnis.','http://www.youtube.com/embed/KWjqicp6Lt0',1,'2020-09-04 07:15:14','2020-09-04 07:15:14'),(10,'Repellendus iste ut exercitationem sunt.','Reprehenderit expedita et ipsum in. Repellendus aliquid odio eos maiores ab eum consequuntur. Quos ut ut et perspiciatis consequuntur. Dolorum atque dolores itaque ut quo corporis.','http://www.youtube.com/embed/KWjqicp6Lt0',1,'2020-09-04 07:15:14','2020-09-04 07:15:14');
/*!40000 ALTER TABLE `videos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `we_offers`
--

DROP TABLE IF EXISTS `we_offers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `we_offers` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `we_offers`
--

LOCK TABLES `we_offers` WRITE;
/*!40000 ALTER TABLE `we_offers` DISABLE KEYS */;
INSERT INTO `we_offers` VALUES (1,'Alias debitis quisquam amet.','Magni vel itaque dicta qui veritatis. Ut autem ut voluptatem cumque minima unde harum. Est accusantium labore cum quasi repudiandae autem. Laboriosam animi officiis autem reprehenderit minus ullam.',1,'2020-09-04 07:15:14','2020-09-04 07:15:14'),(2,'Autem rerum et vero molestias.','Aspernatur occaecati magnam ut eos aut iusto. Voluptas repudiandae inventore qui corporis non possimus cumque consequuntur. Error voluptates incidunt eos esse qui eum ipsum reprehenderit.',1,'2020-09-04 07:15:14','2020-09-04 07:15:14'),(3,'Accusantium amet aperiam nam eligendi adipisci.','Nulla nobis debitis minima at cum commodi dolor. Voluptatem molestias modi eligendi ab qui. Officia veritatis quia fuga nostrum mollitia et quo.',1,'2020-09-04 07:15:14','2020-09-04 07:15:14');
/*!40000 ALTER TABLE `we_offers` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-09-04  7:25:56
