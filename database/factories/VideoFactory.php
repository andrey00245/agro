<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(\App\Video::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence(5),
        'url' => 'http://www.youtube.com/embed/KWjqicp6Lt0',
        'description' => $faker->text,
        'is_active' => true,
    ];
});
