<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(\App\StaticPage::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence(5),
        'description_block1' => $faker->text,
        'description_block2' => $faker->text,
        'description_block3' => $faker->text,
        'image' => $faker->image(),
    ];
});
