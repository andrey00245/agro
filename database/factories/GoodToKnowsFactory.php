<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(\App\GoodToKnow::class, function (Faker $faker) {
    return [
        'title' => $faker->name,
        'description_block1' => $faker->text,
        'description_block2' => $faker->text(1000),
        'description_block3' => $faker->text(1000),
        'edition' => $faker->firstName .' '. $faker->lastName,
        'is_active' => true,
    ];
});
