<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(\App\Contact::class, function (Faker $faker) {
    return [
        'phone' => $faker->phoneNumber,
        'email' => $faker->email,
        'address' => 'test address',
        'whatsapp' => '+7 (978) 881-73-97',
        'instagram' => '#',
        'youtube' => '#',
        'map' => '              <iframe width="520" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" id="gmap_canvas" src="https://maps.google.com/maps?width=520&amp;height=400&amp;hl=en&amp;q=%D0%93.%20%D0%A1%D0%B5%D0%B2%D0%B0%D1%81%D1%82%D0%BE%D0%BF%D0%BE%D0%BB%D1%8C,%20%D0%A3%D0%BB.%20%D0%90%D0%B4%D0%BC%D0%B8%D1%80%D0%B0%D0%BB%D0%B0%20%D0%AE%D0%BC%D0%B0%D1%88%D0%B5%D0%B2%D0%B0,%204%D0%B2%20%D0%A1%D0%B5%D0%B2%D0%B0%D1%81%D1%82%D0%BE%D0%BF%D0%BE%D0%BB%D1%8C+()&amp;t=&amp;z=16&amp;ie=UTF8&amp;iwloc=B&amp;output=embed"></iframe>
              <a href=\'https://embed-map.org/\'>
                add google map to my website</a> 
              <script type=\'text/javascript\' src=\'https://maps-generator.com/google-maps-authorization/script.js?id=8cfb051dfd40ce6d13e0fdcfb0475e48103a9262\'></script>',

    ];
});
