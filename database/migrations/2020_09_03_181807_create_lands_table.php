<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lands', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->text('description');
            $table->string('area')->nullable();
            $table->string('cost')->nullable();
            $table->string('cost_for_object')->nullable();
            $table->text('map')->nullable();
            $table->text('video_url')->nullable();
            $table->string('video_title')->nullable();
            $table->text('video_description')->nullable();
            $table->boolean('is_active')->default(true);
            $table->string('slug')->nullable();

            $table->unsignedBigInteger('country_land_id')->nullable();

            $table->foreign('country_land_id')
                ->references('id')
                ->on('country_lands');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lands');
    }
}
