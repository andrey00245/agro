<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGoodToKnowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('good_to_knows', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->text('description_block1')->nullable();
            $table->text('description_block2')->nullable();
            $table->text('description_block3')->nullable();
            $table->boolean('is_active')->default(true);
            $table->string('edition')->nullable();
            $table->string('slug')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('good_to_knows');
    }
}
