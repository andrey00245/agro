<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(DocumentSeeder::class);
        $this->call(FaqSeeder::class);
        $this->call(GoodKnowSeeder::class);
        $this->call(LandsSeeder::class);
        $this->call(PressFactorySeeder::class);
        $this->call(UserQuestionSeeder::class);
        $this->call(VideoSeeder::class);
        $this->call(WeOfferSeeder::class);
        $this->call(UsersSeeder::class);
        $this->call(ContactSeeder::class);
        $this->call(StaticPageSeeder::class);
    }
}
