<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class VideoSeeder extends Seeder
{
    public function run()
    {
        $categoryVideos = factory(\App\CategoryVideo::class, 5)->create();


        $categoryVideos->each(function (\App\CategoryVideo $categoryVideo) {
            $categoryVideo->videos()->saveMany(factory(\App\Video::class, rand(1, 5))->make([
                'category_video_id' => $categoryVideo->id
            ]));
        });
    }
}
