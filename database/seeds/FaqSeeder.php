<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class FaqSeeder extends Seeder
{
    public function run()
    {
        factory(\App\Faq::class, 10)->create();
    }
}
