<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class UserQuestionSeeder extends Seeder
{
    public function run()
    {
        factory(\App\UserQuestion::class, 10)->create();
    }
}
