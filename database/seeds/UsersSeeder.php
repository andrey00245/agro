<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    public function run()
    {
        \App\User::create([
            'name' => 'Account',
            'password' => \Illuminate\Support\Facades\Hash::make('password'),
            'email' => 'admin@example.com',
        ]);
    }
}
