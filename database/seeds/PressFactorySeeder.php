<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class PressFactorySeeder extends Seeder
{
    public function run()
    {
        factory(\App\Press::class, 10)->create();
    }
}
