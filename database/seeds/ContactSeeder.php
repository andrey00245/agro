<?php

use Illuminate\Database\Seeder;

class ContactSeeder extends Seeder
{
    public function run()
    {
        factory(\App\Contact::class, 1)->create();
    }
}
