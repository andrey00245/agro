<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class DocumentSeeder extends Seeder
{
    public function run()
    {
        factory(\App\Document::class, 10)->create();
    }
}
