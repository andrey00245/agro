<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class WeOfferSeeder extends Seeder
{
    public function run()
    {
        factory(\App\WeOffer::class, 3)->create();
    }
}
