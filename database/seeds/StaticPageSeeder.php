<?php

use Illuminate\Database\Seeder;

class StaticPageSeeder extends Seeder
{
    public function run()
    {
        factory(\App\StaticPage::class, 6)->create();
    }
}
