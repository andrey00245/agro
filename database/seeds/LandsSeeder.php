<?php

use Illuminate\Database\Seeder;

class LandsSeeder extends Seeder
{
    public function run()
    {
        factory(\App\Land::class, 10)->create();
    }
}
