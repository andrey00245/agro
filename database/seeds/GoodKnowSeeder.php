<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class GoodKnowSeeder extends Seeder
{
    public function run()
    {
        factory(\App\GoodToKnow::class, 10)->create();
    }
}
