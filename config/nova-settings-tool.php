<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Settings Path
    |--------------------------------------------------------------------------
    |
    | Path to the JSON file where settings are stored.
    |
    */

    'path' => storage_path('app/settings.json'),

    /*
    |--------------------------------------------------------------------------
    | Sidebar Label
    |--------------------------------------------------------------------------
    |
    | The text that Nova displays for this tool in the navigation sidebar.
    |
    */

    'sidebar-label' => 'Settings',

    /*
    |--------------------------------------------------------------------------
    | Title
    |--------------------------------------------------------------------------
    |
    | The browser/meta page title for the tool.
    |
    */

    'title' => 'Settings',

    /*
    |--------------------------------------------------------------------------
    | Settings
    |--------------------------------------------------------------------------
    |
    | The good stuff :). Each setting defined here will render a field in the
    | tool. The only required key is `key`, other available keys include `type`,
    | `label`, `help`, `placeholder`, `language`, and `panel`.
    |
    */

    'settings' => [

        [
            'key' => 'about_seo',
            'label' => 'Про Эдуарда',
            'panel' => 'Settings',
            'type'=> 'textarea',
        ],
        [
            'key' => 'fio',
            'label' => 'ФИО',
            'panel' => 'План первого этапа (Про Эдуарда)',
            'type'=> 'text',
        ],
        [
            'key' => 'position',
            'label' => 'Должность',
            'panel' => 'План первого этапа (Про Эдуарда)',
            'type'=> 'text',
        ],
        [
            'key' => 'phone',
            'label' => 'Телефон',
            'panel' => 'План первого этапа (Про Эдуарда)',
            'type'=> 'text',
        ],

        [
            'key' => 'email',
            'label' => 'Email',
            'panel' => 'План первого этапа (Про Эдуарда)',
            'type'=> 'text',
        ],

        [
            'key' => 'description',
            'label' => 'Описание',
            'panel' => 'План первого этапа (Про Эдуарда)',
            'type'=> 'textarea',
        ],
    ],
];
