@extends('layout.other-header')
@section('content')
    <link rel="stylesheet" href="{{asset('/styles/reset.css')}}"/>

    <section class="land-main-section">
        <ul class="pagination-wrapper">
            <li>
                <a href="{{route('main')}}">Главная </a>
            </li>
            <li>
                <a>Земельные участки</a>
            </li>
        </ul>
        <h2 class="land-main-title">Земельные участки</h2>
        <div class="card-links-wrapper ">
            <a href="{{route('locality')}}"
               style="background: linear-gradient(0deg, rgba(28, 28, 28, 0.3), rgba(28, 28, 28, 0.3)), url(./img/map.png)"
               class="illion" >Карта <br> Участков</a>
            @foreach($static_pages as $page)
                <a href="{{route('static-page',['slug' => $page->slug])}}"
                   style="background: linear-gradient(0deg, rgba(28, 28, 28, 0.3), rgba(28, 28, 28, 0.3)), url({{$page->getMainImage()}})"
                   class="illion"
                >{{$page->title}}</a>
            @endforeach

        </div>
    </section>
    <section class="commercial-offers-section">
        <div class="commercial-offers-inner">
            <img class="commercial-offers-icon" src="./images/about-icon.png" alt="about-icon"/>
            <h4 class="commercial-offers-title">Коммерческие предложения земельных участков</h4>
            <div class="commercial-offers-sort-box">
                <a href="{{route('locality')}}" class="all-map">Посмотреть на карте</a>
                <div class="custom-select">
                    <form class="land-form" method="GET" action="{{route('land-sort')}}">
                        <div class="select-arrow"></div>
                        <select class="land-sort" name="sort">
                            <option value="0">Сортировать по: <span class="strong-option"> Все</span></option>
                            @foreach($countries as $country )
                                <option {{request()->query('sort') && request()->query('sort') == $country->id ? 'selected' : ''  }} value="{{$country->id}}">
                                    <span class="strong-option"> {{$country->name}}</span></option>
                            @endforeach
                        </select>
                    </form>
                </div>
            </div>
            <div class="product-list-container">
                @foreach($lands as $land)
                    <div class="product-item">
                        <img src="{{asset($land->getMainImage())}}" style="width:360px; height: 330px;"
                             alt="product-foto">
                        <h6 class="product-item-title">{{$land->getShortTitle()}}</h6>
                        <div class="product-item-box">
                            <div>
                                Площадь: {{$land->area}} Га
                            </div>
                            <div>
                                Цена за обьект: <span> {{$land->cost_for_object}} руб</span>
                            </div>
                        </div>
                        <p class="product-item-description">
                            {{$land->getShortDescription()}}
                        </p>
                        <a class="go-product-link" href="{{route('land',['slug' => $land->slug])}}">Смотреть
                            подробнее</a>
                    </div>
                @endforeach
            </div>
        </div>
        @if(isset($is_more) && $is_more)
            <a href="{{route('all-lands')}}" class="more-button">Больше Участков</a>
        @endif
    </section>
    <a href="tel:+{{\App\Contact::first()->land_phone}}" class="main-footer-btn get-more-lands-btn">Больше земельныхучастков {{\App\Contact::first()->land_phone}}</a>

    </div>
    <script
            type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script>
        $(document).on('change', '.land-sort', function () {
            $('.land-form').submit();
        });
    </script>
@stop
