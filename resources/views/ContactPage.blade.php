@extends('layout.other-header')
@section('content')
    <link rel="stylesheet" href="{{asset('/styles/reset.css')}}"/>
    <section class="document-main-section good-know-page">
        <ul class="pagination-wrapper">
            <li>
                <a href="{{route('main')}}">Главная </a>
            </li>
            <li>
                <a>Контакты</a>
            </li>
        </ul>
        <div class="document-main-inner">
            <h2 class="document-main-title">
                Контакты
            </h2>
            <div class="contact-inner-conadd google map to my websitetainer">
                <ul class="social-links-container">
                    <li>
                        <a href="tel:{{$contact->phone}}">
                            <img class="contact-phone" src="{{asset('/images/phone-call.png')}}" alt="phone-call">
                            Телефон : {{$contact->phone}}
                        </a>
                    </li>
                    <li>
                        <a href="mailto:{{$contact->email}}">
                            <img class="contact-email" src="{{asset('/images/mail-icon.png')}}" alt="mail-icon">
                            Email : {{$contact->email}}
                        </a>
                    </li>
                    <li>
                        <img class="contact-maps" src="{{asset('/images/maps-and-flags.png')}}" alt="maps-and-flags">
                        {{$contact->address}}
                    </li>
                    <li>
                        <a href="https://wa.me/{{$contact->getValidNumber()}}" target="_blank">
                            <img class="contact-whatsapp" src=".{{'/images/whatsapp.png'}}" alt="whatsapp">
                            Whatsapp : {{$contact->whatsapp}}
                        </a>
                    </li>
                </ul>
                <div class="divider">
                </div>
                <h2 class="document-main-title">
                 <a href="{{route('winemaker')}}">
                     <img  style="width: 250px" src="{{asset('/images/vino-contact.jpeg')}}" alt="vine-logo">
                 </a>
                </h2>
                <ul class="social-links-container">
                    <li>
                        <a href="tel:{{$contact->agro_phone}}">
                            <img class="contact-phone" src="{{asset('/images/phone-call.png')}}" alt="phone-call">
                            Телефон : {{$contact->agro_phone}}
                        </a>
                    </li>
                    <li>
                        <a href="mailto:{{$contact->agro_email}}">
                            <img class="contact-email" src="{{asset('/images/mail-icon.png')}}" alt="mail-icon">
                            Email : {{$contact->agro_email}}
                        </a>
                    </li>
                    <li>
                        <img class="contact-maps" src="{{asset('/images/maps-and-flags.png')}}" alt="maps-and-flags">
                        {{$contact->agro_address}}
                    </li>
                    <li>
                        <a href="https://wa.me/{{$contact->getValidAgroNumber()}}" target="_blank">
                            <img class="contact-whatsapp" src=".{{'/images/whatsapp.png'}}" alt="whatsapp">
                            Whatsapp : {{$contact->agro_whatsapp}}
                        </a>
                    </li>
                </ul>
                <div class="contact-map-container">
                    {!! $contact->map !!}
                </div>
            </div>
        </div>
    </section>
@stop
