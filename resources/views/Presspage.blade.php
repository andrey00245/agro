@extends('layout.other-header')
@section('content')
    <link rel="stylesheet" href="{{asset('/styles/reset.css')}}"/>

    <section class="document-main-section good-know-page">
        <ul class="pagination-wrapper">
            <li>
                <a href="{{route('main')}}">Главная </a>
            </li>
            <li>
                <a >Пресса о нас</a>
            </li>
        </ul>
        <div class="document-main-inner">
            <h2 class="document-main-title">
                Пресса о нас
            </h2>
            <div class="press-list-container">
                @foreach($presses as $press)
                    <a target="_self" href="{{$press->url}}">
                        <div class="press-item">
                            <div class="press-item-photo">
                                <img src="{{$press->getMainImage()}}" alt="">
                            </div>
                            <div class="press-item-content-box">
                                <div class="document-title">
                                    {{$press->title}}
                                </div>
                                <p class="document-description">
                                    {{$press->created_at}}
                                </p>
                                <p class="document-description">
                                    {{$press->getShortDescription()}}
                                </p>
                            </div>
                        </div>
                    </a>
                @endforeach
            </div>
        </div>
    </section>
@stop
