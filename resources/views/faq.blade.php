@extends('layout.other-header')
@section('content')
    <link rel="stylesheet" href="{{asset('/styles/reset.css')}}"/>

    <section class="product-main-section">
        <ul class="pagination-wrapper">
            <li>
                <a href="{{route('main')}}">Главная </a>
            </li>
            <li>
                <a>FAQ</a>
            </li>
        </ul>
        <div class="inner-page-container faq-page">
            <div class="inner-page-container-bg">
            </div>
            <h3 class="timplate-page-title">FAQ</h3>
            <div class="faq-content-container">
                <p class="faq-sub-title">
                    В этом разделе вы можете увидеть, самые часто задаваемы вопросы и найти ответ для себя. Или задать
                    нам и мы поможем разобраться в вашей ситуации.
                </p>
                <form action="{{route('user-question')}}" method="POST" class="faq-question-container">
                    @csrf
                    <input name='question' type="text" placeholder="Задать Вопрос....">
                    <button>Отправить</button>
                </form>
                @if(session()->has('question_sent'))
                    <br>
                    <br>
                    <strong style="color: green">{{session()->get('question_sent')}}</strong>
                @endif

                <div class="faq-question-list-container">
                    <div class="faq-question-column">
                        @foreach($faqs as $faq)
                            @if($loop->index % 2 === 0)
                                <div class="faq-question-item">
                                    <div class="faq-question">
                                        <h6>{{$faq->question}}</h6>
                                        <div class="faq-icon">
                                        </div>
                                    </div>
                                    <div class="faq-answer">
                                        <p>
                                            {{$faq->answer}}
                                        </p>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    </div>
                    <div class="faq-question-column">
                        @foreach($faqs as $faq)
                            @if($loop->index % 2 != 0)
                                <div class="faq-question-item">
                                    <div class="faq-question">
                                        <h6>{{$faq->question}}</h6>
                                        <div class="faq-icon">
                                        </div>
                                    </div>
                                    <div class="faq-answer">
                                        <p>
                                            {{$faq->answer}}
                                        </p>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop
