@extends('layout.other-header')
@section('content')
    <link rel="stylesheet" href="{{asset('/styles/reset.css')}}"/>

    <section class="document-main-section">
        <ul class="pagination-wrapper">
            <li>
                <a href="{{route('main')}}">Главная </a>
            </li>
            <li>
                <a href="{{route('winemaking')}}">Виноделие</a>
            </li>
            <li>
                <a>Документы</a>
            </li>
        </ul>
        <div class="inner-page-container">
            <div class="title-container">
                <h3 class="timplate-page-title">Библиотека</h3>
                <div class="custom-select">
                    <form class="video-form" method="GET" action="{{route('articles-sort')}}">
                        <div class="select-arrow"></div>
                        <select name="category" id="video-filter" class="partner-category">
                            <option value="{{null}}">Все</option>
                            @foreach($categories as $category)
                            <option value="{{$category->id}}" {{request()->query('category') && request()->query('category') == $category->id ? 'selected' : ''  }}>
                                Сортировать по: <span class="strong-option"> {{$category->name}}</span>
                            </option>
                            @endforeach
                        </select>
                    </form>
                </div>
            </div>
            <div class="library-items-list">
                @foreach($articles as $article)
                <a  href="{{$article->file ?? '#'}}" {{$article->file ? 'download' : '-'}}>
                    <div class="library-item-box">
                        <div class="library-item-image">
                            <img src="{{$article->image}}" alt="Rectangle_144">
                        </div>
                        <div class="library-item-content">
                            <h6 class="library-item-title">
                                {{$article->title}}
                            </h6>
                            <p class="library-item-sub-title">
                                {{ \Jenssegers\Date\Date::parse($article->created_at)->format('j F Y') }} года
                            </p>
                            <p class="library-item-description">
                                {{$article->description}}
                            </p>
                        </div>
                    </div>
                </a>
                @endforeach
            </div>

            @foreach($articles as $article)
            <div class="library-items-list adaptive">
                <div class="library-item-box">
                    <div class="library-item-image">
                        <img src="{{$article->image}}" alt="Rectangle_144">
                    </div>
                    <div class="library-item-content">
                        @if($article->file)
                        <a href="{{$article->file}}" download>
                            <img src="./images/download-icon.png" alt="adaptive-download-icon">
                        </a>
                        @endif
                        <h6 class="library-item-title">
                            {{$article->title}}
                        </h6>
                        <p class="library-item-sub-title">
                            {{ \Jenssegers\Date\Date::parse($article->created_at)->format('j F Y') }} года
                        </p>
                        <p class="library-item-description">
                            {{$article->description}}
                        </p>
                    </div>
                </div>
            </div>
            @endforeach
            <div class="library-go-btn">
                <a href="{{route('lands')}}" class="go-to-library">Земельные участки</a>
            </div>
        </div>
    </section>

    <script
            type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script>
        $(document).on('change', '.partner-category', function (){
            $('.video-form').submit();
        });
    </script>
@stop