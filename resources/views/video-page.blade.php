@extends('layout.other-header')
@section('content')
    <link rel="stylesheet" href="{{asset('/styles/reset.css')}}"/>

    <section class="product-main-section">
        <ul class="pagination-wrapper">
            <li>
                <a href="{{route('main')}}"> Главная </a>
            </li>
            <li>
                <a>Видео</a>
            </li>
        </ul>
        <div class="inner-page-container">
            <div class="title-container">
                <h3 class="timplate-page-title">Видео</h3>
                <div class="custom-select">
                    <form class="video-form" method="GET" action="{{route('sort')}}">
                        <div class="select-arrow"></div>
                        <select name="sort_by" id="video-filter">
                            <option value="0">Все</option>
                            @foreach($categories as $category)
                                <option {{request()->query('sort_by') && request()->query('sort_by') == $category->id ? 'selected' : ''  }} value="{{$category->id}}">
                                    Сортировать по: <span class="strong-option"> {{$category->name}}</span></option>
                            @endforeach
                        </select>
                    </form>
                </div>
            </div>
            <div class="video-list-container">
                @foreach($videos as $video)
                    <div class="video-item">
                        <iframe
                                src="{{$video->url}}"
                                width="100"
                                frameborder="0"
                                allowfullscreen
                        ></iframe>
                        <h5 class="video-item-title">
                            {{$video->title}}
                        </h5>
                        <p class="video-item-description">
                            {{$video->description}}
                        </p>
                    </div>
                @endforeach
            </div>
        </div>
        @if(isset($is_more) && $is_more)
            <a href="{{route('all-videos')}}" class="more-button">Больше Видео</a>
        @endif
    </section>
    <script
            type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js
  "
    ></script>
    <script>
        $(document).on('change', '#video-filter', function () {
            $('.video-form').submit();
        });
    </script>
@stop

