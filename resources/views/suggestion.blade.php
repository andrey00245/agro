@extends('layout.other-header')
@section('content')
    <link rel="stylesheet" href="{{asset('/styles/reset.css')}}"/>

    <section class="document-main-section good-know-page">
        <ul class="pagination-wrapper">
            <li>
                <a href="{{route('main')}}">Главная </a>
            </li>
            <li>
                <a href="{{route('lands')}}">Земельные участки</a>
            </li>
            <li>
                <a href="{{url()->previous()}}" >Строительство на сельхозземле</a>
            </li>
            <li>
                <a>Специальные Предложения</a>
            </li>
        </ul>
        <div class="document-main-inner">
            <h2 class="document-main-title">
                Специальные Предложения
            </h2>
            <div class="document-list-container">
                @foreach($suggestions as $suggestion)
                    <div class="document-item">
                        <a href="{{$suggestion->getFile() ?? '#'}}">
                            <div class="document-title">
                                {{$suggestion->title}}
                            </div>
                        </a>
                        <p class="document-description">
                            {{$suggestion->created_at}}
                        </p>
                        <p class="document-description">
                            {{$suggestion->getShortDescription()}}
                        </p>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@stop
