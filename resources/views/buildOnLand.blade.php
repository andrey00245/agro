@section('title')
    {{$static_page->meta_title}}
@stop

@section('description')
    {{$static_page->meta_description}}
@stop

@section('keywords')
    {{$static_page->meta_keywords}}
@stop

@extends('layout.other-header')
@section('content')
    <section class="product-main-section">
        <ul class="pagination-wrapper">
            <li>
                <a href="{{route('main')}}">Главная </a>
            </li>
            <li>
                <a href="{{route('lands')}}">Земельные участки</a>
            </li>
            <li>
                <a>{{$static_page->title}}</a>
            </li>
        </ul>
        <div class="inner-page-container">
            <div class="inner-page-container-bg">
            </div>
            <h3 class="timplate-page-title">{{$static_page->title}}</h3>

            @if($static_page->posts()->exists())
                <div class="workers-list-wrapper">
                    @foreach($static_page->posts as $post)
                        <div class="worker-item-box">
                            <div class="worker-image">
                                <img src="{{$post->image}}" alt="">
                            </div>
                            <div class="vertical-devider">
                            </div>
                            <div class="worker-content-box">
                                <h6>{{$post->title}}</h6>
                                <p>{{$post->sub_title}}</p>
                                <p>
                                    {{$post->description}}
                                </p>
                            </div>
                        </div>
                    @endforeach
                </div>
            @endif
            {{--                        <div class="links-container">--}}
            {{--                            <a href="{{route('static-page',['slug' => $winemark->slug])}}" class="build-link-item">--}}
{{--                                {{$winemark->title}}--}}
{{--                                <div></div>--}}
{{--        ^                    </a>--}}
{{--                            <a href="{{route('static-page',['slug' => $illeon_system->slug])}}" class="build-link-item">--}}
{{--                                {{$illeon_system->title}}--}}
{{--                                <div></div>--}}
{{--                            </a>--}}
{{--                            <a href="{{route('goodKnow')}}" class="build-link-item">--}}
{{--                                Полезно знать--}}
{{--                                <div class="test"></div>--}}
{{--                            </a>--}}
{{--                        </div>--}}

            <div class="inner-content-container">
                <div class="inner-content-description padding">
                    {!! $static_page->description_block1 !!}
                </div>
                <img class="timplate-image" src="{{$static_page->getMainImage()}}" alt="home-image">
                <div class="initiate-conent-box">
                    <div class="inner-content-description">
                        {!! $static_page->description_block2 !!}
                    </div>
                    <div class="inner-content-description">
                        {!! $static_page->description_block3 !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop
