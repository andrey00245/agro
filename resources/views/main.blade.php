@extends('layout.main-header')
@section('content')
    <link rel="stylesheet" href="{{asset('/styles/reset.css')}}"/>

    <div class="main-title-container">
        <h1 class="title">
            Виноградник Усадьба <br/>
            Фруктовый Сад
        </h1>
        <h3 class="sub-title">Земельный участок в Крыму и Севастополе</h3>
        <a href="{{route('winemaking')}}" class="main-title-btn">Стань виноделом </a>
    </div>
    
    </section>
    <section class="suggest-section-wrapper">
        <div class="suggest-section-inner">
            <h5 class="suggest-title">Мы предлагаем</h5>
            <div class="suggest-list-container">
                @foreach($weOffer as $offer)
                    <div class="suggest-item">
                        <img src="{{$offer->image}}" alt="suggest-item">
                        <h6 class="suggest-item-title">{{$offer->title}}</h6>
                        <p class="suggest-item-description">
                            {{$offer->description}}
                        </p>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
    <section class="about-main-section-wrapper">
        <div class="about-main-section-inner">
            <img src="./images/about-icon.png" alt="about-icon" class="about-icon"/>
            <h4 class="about-main-title">Эдуард Журавлев</h4>
            <p class="about-main-sub-title">
                {{ (new \App\Support\DynamicSettings())->getAboutSeo()  }}
            </p>
            <img class="eduard-image" src="{{asset('images/about-main-img.png')}}" alt="about-main-img"/>
            <p class="about-main-description">
                Приветствую всех, кто любит Крым, море и хочет жить и работать в Севастополе и Крыму
            занимаясь интересным бизнесом.
            </p>
        </div>
    </section>
    <div class="main-slider-wrapper">
        <div class="main-slider-inner">
            @foreach($videos as $video)
                <div class="slider-item">
                    <iframe
                            src="{{$video->url}}"
                            width="100"
                            frameborder="0"
                            allowfullscreen
                    ></iframe>
                </div>
            @endforeach
        </div>
    </div>
    <section class="main-footer-section-wrapper">
        <div class="main-footer-inner">
            <p class="main-footer-description">
               {{$contact->main_text}}
            </p>
            <a href="{{route('press')}}" class="main-footer-btn">Пресса о нас</a>
        </div>
    </section>
@stop
