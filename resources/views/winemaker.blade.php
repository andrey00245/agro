@extends('layout.winemaking-header')
@section('content')
    <link rel="stylesheet" href="{{asset('/styles/reset.css')}}"/>
    <section class="vine-content-wrapper">
        <div class="vine-title-box">
            <span class="yellow">
                Журнал 
            </span>
            pro <br />
            <span class="second yellow">
                виноделие, 
            </span><br />
            <span class="third yellow">
                виноградарство
            </span><br />
            <span class="four">
                и
            </span>
            <br />
            <span class="five">
                субсидии
            </span>
        </div>
        <div class="vine-slider-box">
            @foreach($sliders as $slider)
                <div class="vine-slider-item"
                     style="background:  linear-gradient(0deg, rgba(0, 0, 0, 0.61) 0%, rgba(0, 0, 0, 0) 43.54%), url('{{asset($slider->image)}}');">
                    <h5>
                        {{$slider->title}}
                    </h5>
                </div>
            @endforeach
        </div>
        <ul class="vine-social-box">
            <li>
                <a href="{{$contact->agro_youtube}}" target="_blank">
                    <img class="vine-you-tube" src="../images/yellow-you-tube-icon.png" alt="icon">
                </a>
            </li>
            <li>
                <a href="{{$contact->agro_instagram}}" target="_blank">
                    <img class="vine-instagram" src="../images/yellow-instagram-icon.png" alt="icon">
                </a>
            </li>
            <li>
                <a href="{{$contact->agro_telegram}}" target="_blank">
                    <img class="vine-telegram" src="../images/yellow-tg-icon.png" alt="icon">
                </a>
            </li>
            <li>
                <a href="{{$contact->agro_facebook}}" target="_blank">
                    <img class="vine-fb"  src="../images/yellow-fb-icon.png" alt="icon">
                </a>
            </li>
        </ul>
    </section>
    <section class="vine-bottom-wrapper">
        <div class="vine-desktop-style">
            <div class="vine-bottom-left-image">
                <img src='../images/car-image.png' alt="car-image">
            </div>
            <div class="vine-bottom-slider-box">
                @foreach($articles as $article)
                    <p>
                        {{$article->description}}
                    </p>
                @endforeach
            </div>  
        </div>
        <div class='vine-right-box'>
            <a href="{{route('articles')}}">
                <p>
                    Все материалы
                </p>
                <img src='../images/material-right-arrow.png' alt="material-right-arrow">
            </a>
        </div>
    </section>
@stop
