@section('title')
    {{$static_page->meta_title}}
@stop

@section('description')
    {{$static_page->meta_description}}
@stop

@section('keywords')
    {{$static_page->meta_keywords}}
@stop
@extends('layout.other-header')
@section('content')
    <section class="product-main-section">
        <ul class="pagination-wrapper">
            <li>
                <a href="{{route('main')}}">Главная </a>
            </li>
            <li>
                <a href="{{route('lands')}}">Земельные Участки</a>
            </li>
            <li>
                <a>{{$static_page->title}}</a>
            </li>
        </ul>
        <div class="inner-page-container">
            <div class="inner-page-container-bg">

            </div>
            <h3 class="timplate-page-title">{{$static_page->title}}</h3>
            <div class="inner-content-container" style="font: revert!important;">
                <div class="inner-content-description padding">
                    {!! $static_page->description_block1 !!}
                </div>
                <img class="timplate-image" src="{{$static_page->getMainImage()}}" alt="timplate-image">
                <div class="initiate-conent-box">
                    <div class="inner-content-description">
                        {!! $static_page->description_block2 !!}
                    </div>
                    <div class="inner-content-description">
                        {!! $static_page->description_block3 !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop
