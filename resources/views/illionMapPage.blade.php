@extends('layout.other-header')
@section('content')
    <link rel="stylesheet" href="{{asset('/styles/reset.css')}}"/>

    <section class="product-main-section">
        <ul class="pagination-wrapper">
            <li>
                <a href="{{route('main')}}">Главная </a>
            </li>
            <li>
                <a href="{{route('lands')}}">Продажа земельных участков</a>
            </li>
        </ul>
        <h3 class="adaptive-title">Земельные участки</h3>
        <div class="illion-map-container">
            {!! $contact->location !!}
        </div>
    </section>
@stop
