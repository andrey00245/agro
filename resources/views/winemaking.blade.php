@section('title')
    {{$static_page->meta_title}}
@stop

@section('description')
    {{$static_page->meta_description}}
@stop

@section('keywords')
    {{$static_page->meta_keywords}}
@stop
@extends('layout.other-header')
@section('content')
    <link rel="stylesheet" href="{{asset('/styles/reset.css')}}"/>

    <section class="product-main-section">
        <ul class="pagination-wrapper">
            <li>
                <a href="{{route('main')}}">Главная </a>
            </li>
            <li>
                <a>Виноградарство</a>
            </li>
        </ul>
        <div class="inner-page-container">
            <div class="winemaking-bg">
            </div>
            <h3 class="timplate-page-title">Виноделие</h3>
            <div class="winemaking-cards-container my-dick">
                <a href="{{route('special-offer')}}" class="winemaking-card-item"
                   style="background: linear-gradient(0deg, rgba(28, 28, 28, 0.3), rgba(28, 28, 28, 0.3)), url('{{$static_page->getMainImage()}}');">
                    <h4>
                        Виноградник под ключ
                    </h4>
                </a>
                <a href="{{route('static-page',['slug' => $first_step->slug])}}" class="winemaking-card-item"
                   style="background: linear-gradient(0deg, rgba(28, 28, 28, 0.3), rgba(28, 28, 28, 0.3)), url('{{asset('images/Rectangle-147.png')}}');">
                    <h4>
                        План первого <br> этапа
                    </h4>
                </a>
                <a href="{{route('documents')}}" class="winemaking-card-item"
                   style="background: linear-gradient(0deg, rgba(28, 28, 28, 0.3), rgba(28, 28, 28, 0.3)), url('{{asset('images/document.jpg')}}');">
                    <h4>
                        Документы
                    </h4>
                </a>

                <a href="{{route('static-page',['slug' => $vino->slug])}}" class="winemaking-card-item"
                   style="background: linear-gradient(0deg, rgba(28, 28, 28, 0.3), rgba(28, 28, 28, 0.3)), url('{{$static_page->getMainImage()}}');">
                    <h4>
                        {{$vino->title}}
                    </h4>
                </a>
            </div>
            <div class="winemaking-content-container">
                <img src="./images/about-icon.png" alt="about-icon">
                <div class="winemaking-description ">
                  {!!$contact->vinomarking !!}
                </div>
            </div>
        </div>
    </section>
@stop
