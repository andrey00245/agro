<nav class="burger-menu-container">
    <div class="burger-menu-title">Меню</div>
    <ul class="navigation-list">
        <li>
            <a href="{{route('lands')}}">Земельные участки</a>
        </li>
        <li>
            <a href="{{route('winemaking')}}">Виноградарство</a>
        </li>
        <li>
            <a href="{{route('articles')}}">Библиотека</a>
        </li>
        <li>
            <a href="{{route('video')}}">Видео</a>
        </li>
        <li>
            <a href="{{route('contact')}}">Контакты</a>
        </li>
    </ul>
</nav>
<ul class="burger-menu-social-list">
    <li class="burger-menu-social">
        <a href="tel:{{$contact->phone}}"><img src="{{asset('/images/phone-icon.png')}}" alt="phone-icon"/></a>
    </li>
    <li class="burger-menu-social">
        <a href="{{$contact->instagram}}"><img src="{{asset('/images/instagram-icon.png')}}" alt="phone-icon"/></a>
    </li>
    <li class="burger-menu-social">
        <a href="https://wa.me/{{$contact->getValidNumber()}}" target="_blank"
        ><img src="{{asset('/images/wats-up-icon.png')}}" alt="phone-icon"
            /></a>
    </li>
    <li class="burger-menu-social">
        <a href="{{$contact->youtube}}"><img src="{{asset('/images/you-tube-icon.png')}}" alt="phone-icon"/></a>
    </li>
</ul>
