<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <link rel="stylesheet" href="{{'/styles/reset.css'}}"/>
    <link rel="stylesheet" href="{{'/fonts/fonts.css'}}"/>
    <link rel="stylesheet" type="text/css" href="{{'/slick/slick.css'}}"/>
    <link rel="stylesheet" type="text/css" href="{{'/slick/slick-theme.css'}}"/>
    <link rel="stylesheet" href="{{'/styles/main.css'}}"/>
    <link href="http://fonts.cdnfonts.com/css/roboto" rel="stylesheet">

    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<meta name="yandex-verification" content="d9c828845b872ebd" />


    <title>{{$seo->meta_title}}</title>
    <meta name="description" content="{{$seo->meta_description}}">
    <meta name="keywords" content="{{ $seo->meta_keywords}}">

    <meta name="twitter:card" content="">
    <meta name="twitter:site" content="{{url()->current()}}">
    <meta name="twitter:creator" content="">
    <meta name="twitter:title" content="{{$seo->meta_title}}">
    <meta name="twitter:description" content="{{$seo->meta_description}}">
    <meta name="twitter:image" content="{{'images/illion-logo.png'}}">

    <meta property="og:url" content="{{url()->current()}}">
    <meta property="og:title" content="{{$seo->meta_title}}">
    <meta property="og:description" content="{{$seo->meta_description}}">
    <meta property="og:type" content="website">
    <meta property="og:image" content="{{'images/footer-logo.png'}}">
</head>
<body>

<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

   ym(67958008, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
   });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/67958008" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

<div class="page-wrapper">
    <div class="page-inner-wrapper">
        <div class="burger-menu-wrapper">
            <div class="burger-menu-inner">
                <header class="header-wrapper">
                    <div class="header-logo-container">
                        <a href="{{route('main')}}" class="header-logo">
                            <img src="{{asset('/img/black-logo.svg')}}" alt="header-logo" />
{{--                            <span> ИЛИОН </span>--}}
                        </a>
                    </div>
                    <div class="burger-menu-close">
                        <div class="first"></div>
                        <div class="second"></div>
                    </div>
                </header>
                @include('layout.burger-menu')
            </div>
        </div>
        <section class="main-section">
            <header class="header-wrapper desktop">
                <div class="header-logo-container">
                    <a href="{{route('main')}}" class="header-logo">
                        <img src="{{asset('/img/black-logo.svg')}}" alt="header-logo" />
{{--                        <span> ИЛИОН </span>--}}
                    </a>
                </div>
                <nav class="header-navication-wrapper">
                    <ul class="header-navication-box">
                        <li class="header-navigation-item"><a href="{{route('lands')}}">Земельные участки</a></li>
                        <li class="header-navigation-item"><a href="{{route('winemaking')}}">Виноградарство</a></li>
                        <li class="header-navigation-item"><a href="{{route('articles')}}">Библиотека</a></li>
                        <li class="header-navigation-item"><a href="{{route('video')}}">Видео</a></li>
                        <li class="header-navigation-item"><a href="{{route('contact')}}">Контакты</a></li>
                    </ul>
                </nav>
                <div class="burger-menu">
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
            </header>
@yield('content')
@include('layout.footer')
