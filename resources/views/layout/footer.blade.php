<footer class="footer-wrapper">
    <div class="footer-inner">
    <div>
        <a href="{{route('main')}}" class="footer-logo">
            <img src="{{asset('/img/white-logo.svg')}}" alt="footer-logo" />
{{--                    <span> ИЛИОН </span>--}}
        </a>
        <a href="{{asset($contact->image)}}" download class="footer-copyright-title first">
            Политика конфиденциальности
        </a>
        <a href="{{asset($contact->image)}}" download class="footer-copyright-title">
            Защита персональных данных
        </a>
        <a href="{{asset($contact->image)}}" download class="footer-copyright-title">
        &#169; Винодел Крым, 2021. Все права защищены.
        </a>
    </div>

        <nav class="footer-routes-wrapper">
            <ul class="header-navication-box">
                <li class="header-navigation-item"><a href="{{route('lands')}}">Земельные участки</a></li>
                <li class="header-navigation-item"><a href="{{route('partner')}}">Наши партнеры</a></li>
                <li class="header-navigation-item"><a href="{{route('faq')}}">Вопрос-Ответ</a></li>
            </ul>
        </nav>

        <div class="footer-right-box">
            <div class="liveinternet-box">
            <!--LiveInternet counter--><a href="//www.liveinternet.ru/click"
            target="_blank"><img id="licntB2E5" width="88" height="31" style="border:0"
            title="LiveInternet: показано число просмотров и посетителей за 24 часа"
            src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAEALAAAAAABAAEAAAIBTAA7"
            alt=""/></a><script>(function(d,s){d.getElementById("licntB2E5").src=
            "//counter.yadro.ru/hit?t53.6;r"+escape(d.referrer)+
            ((typeof(s)=="undefined")?"":";s"+s.width+"*"+s.height+"*"+
            (s.colorDepth?s.colorDepth:s.pixelDepth))+";u"+escape(d.URL)+
            ";h"+escape(d.title.substring(0,150))+";"+Math.random()})
            (document,screen)</script><!--/LiveInternet-->
            </div>
            <nav class="footer-navigation-wrapper">
                <ul class="footer-navigation-box">
                    <li class="footer-navigation-item">
                        <a href="tel:{{$contact->phone}}"
                        ><img src="{{asset('/images/phone-icon.png')}}" alt="phone-icon"
                            /></a>
                    </li>
                    <li class="footer-navigation-item">
                        <a href="{{$contact->instagram}}">
                            <img src="{{asset('/images/instagram-icon.png')}}" alt="phone-icon"/>
                        </a>
                    </li>
                    <li class="footer-navigation-item">
                        <a href="https://wa.me/{{$contact->getValidNumber()}}" target="_blank"
                        ><img src="{{asset('/images/wats-up-icon.png')}}" alt="phone-icon"
                            /></a>
                    </li>
                    <li class="footer-navigation-item">
                        <a href="{{$contact->youtube}}"><img src="{{asset('/images/you-tube-icon.png')}}" alt="phone-icon"/></a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</footer>
</div>
<script
        type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript" src="{{asset('/slick/slick.min.js')}}"></script>
<script  type="text/javascript" src="{{asset('/js/script.js')}}"></script>
<script type="text/javascript" src="{{asset('/lightbox.js')}}"></script>
</body>
</html>
