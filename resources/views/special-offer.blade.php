<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <link rel="stylesheet" href="{{asset('/css/reset.css')}}" />
    <link rel="stylesheet" href="{{asset('/fonts-offer/fonts.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('slick/slick.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('slick/slick-theme.css')}}" />
    <link rel="stylesheet" href="{{asset('/css/main.css')}}" />
    <link rel="stylesheet" href="{{asset('/styles/main.css')}}" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>{{$seo->meta_title}}</title>
    <meta name="description" content="{{$seo->meta_description}}">
    <meta name="keywords" content="{{ $seo->meta_keywords}}">

    <meta name="twitter:card" content="">
    <meta name="twitter:site" content="{{url()->current()}}">
    <meta name="twitter:creator" content="">
    <meta name="twitter:title" content="{{$seo->meta_title}}">
    <meta name="twitter:description" content="{{$seo->meta_description}}">
    <meta name="twitter:image" content="{{'/img/illion-logo.png'}}">
    <meta property="og:url" content="{{url()->current()}}">
    <meta property="og:title" content="{{$seo->meta_title}}">
    <meta property="og:description" content="{{$seo->meta_description}}">
    <meta property="og:type" content="website">
    <meta property="og:image" content="{{'/img/illion-logo.png'}}">
</head>
<body>
    
    <!-- Yandex.Metrika counter -->
<script type="text/javascript" >
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

   ym(67958008, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
   });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/67958008" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

    
<div class="page-wrapper">
    <div class="page-inner-wrapper">
        <div class="burger-menu-wrapper">
            <div class="burger-menu-inner">
                <header class="header-wrapper">
                    <div class="header-logo-container">
                        <a href="{{route('main')}}" class="header-logo">
                            <img src="{{asset('./img/black-logo.svg')}}" alt="header-logo" />
{{--                            <span> ИЛИОН </span>--}}
                        </a>
                    </div>
                    <div class="burger-menu-close">
                        <div class="first"></div>
                        <div class="second"></div>
                    </div>
                </header>
                <nav class="burger-menu-container">
                    <div class="burger-menu-title">Меню</div>
                    <ul class="navigation-list">
                        <li ><a href="#how-it-works" class="link-to-section hide-menu">Как это работает</a></li>
                        <li ><a href="{{route('lands')}}" >Купить участок</a></li>
                        <li ><a href="#contact-us" class="link-to-section hide-menu">Связаться с нами</a></li>
                    </ul>
                </nav>
                <ul class="burger-menu-social-list">
                    <li class="burger-menu-social">
                        <a href="tel:79788544787"><img src="{{asset('/img/phone-icon.png')}}" alt="phone-icon" /></a>
                    </li>
                    <li class="burger-menu-social">
                        <a href="#"><img src="{{asset('/img/instagram-icon.png')}}" alt="phone-icon" /></a>
                    </li>
                    <li class="burger-menu-social">
                        <a href="https://wa.me/79788544787" target="_blank"
                        ><img src="{{asset('/img/wats-up-icon.png')}}" alt="phone-icon"
                            /></a>
                    </li>
                    <li class="burger-menu-social">
                        <a href="#"><img src="{{asset('/img/you-tube-icon.png')}}" alt="phone-icon" /></a>
                    </li>
                </ul>
            </div>
        </div>
        <header class="header-wrapper desktop">
            <div class="header-logo-container">
                <a href="{{route('main')}}" class="header-logo">
                    <img src="{{asset('./img/black-logo.svg')}}" alt="header-logo" />
                    {{--                    <span> ИЛИОН </span>--}}
                </a>
            </div>
            <nav class="header-navication-wrapper">
                <ul class="header-navication-box">
                    <li class="header-navigation-item"><a href="#how-it-works" class="link-to-section">Как это работает</a></li>
                    <li class="header-navigation-item"><a href="{{route('lands')}}" >Купить участок</a></li>
                    <li class="header-navigation-item"><a href="#contact-us" class="link-to-section">Связаться с нами</a></li>
                </ul>
            </nav>
            <div class="burger-menu">
                <div></div>
                <div></div>
                <div></div>
            </div>
        </header>
        <section class="main-section">
            <div class="main-section-title-container wrapper">
                <h1 class="main-section-title">
                    Создаем <span>под ключ
                </span>
                    Ваш винодельческий
                    и виноградарский
                    проект!
                </h1>
                <h4 class="main-section-sub-title">
                    Снимаем с ваших плеч все тяготы работ на винограднике, поиска подрядчиков и персонала, подготовки и подачи документов на субсидии в государственные органы.
                </h4>
            </div>
            <div class="main-section-content-box">
                <div class="main-section-content-right">
                    <img src="{{asset('/img/main-bg.png')}}" alt="main-bg">
                </div>
                <div class="main-section-content-left">
                    <h6>
                        Илион-Агро
                    </h6>
                    <p>
                        от подбора земельного участка
                    </p>
                    <p>
                        до выхода виноградника в стадию плодоношения.
                    </p>
                    <a href="#contact-us" class="contact-us-btn link-to-section center">
                        Связаться с нами
                    </a>
                </div>
            </div>
        </section>
        <section class="about-us-section">
            <img src="{{asset('/img/about-icon.png')}}" alt="about-icon">
            <p>
                Наша команда это профессиональные агрономы-виноградари,  специалисты по защите растений, управляющие виноградарскими и винодельческими проектами, виноделы-энологи.
            </p>
        </section>
        <section class="selection-garden-sections wrapper">
            <h2>
                Подбор земельного участка
            </h2>
            <div class="selection-cards-wrapper">
                <div class="card-box">
                    <img src="{{asset('/img/click-icon.png')}}" alt="click-icon">
                    <h6>
                        Подбор
                    </h6>
                    <p>Земельные участки из нашей базы и подбор участка в нужном  микротерруаре</p>
                </div>
                <div class="card-box">
                    <img src="{{asset('/img/sun-icon.png')}}" alt="sun-icon">
                    <h6>
                        Формирование
                    </h6>
                    <p>Консолидация паёв и формирование участка подходящей площади</p>
                </div>
                <div class="card-box">
                    <img src="{{asset('/img/check-icon.png')}}" alt="check-icon">
                    <h6>
                        Проверка
                    </h6>
                    <p>Юридическая проверка украинских и российских правоустанавливающих документов.</p>
                </div>
                <div class="card-box">
                    <img src="{{asset('/img/hand-shake.png')}}" alt="hand-shake">
                    <h6>
                        Договоренность
                    </h6>
                    <p>Переговоры с собственниками</p>
                </div>
            </div>
        </section>
        <section class="how-it-works-section wrapper" id="how-it-works">
            <div class="how-it-work-inner">
                <h2>
                    Как это работает
                </h2>
                <p class="sub-title">
                    Мы расскажем вам полный этап начиная от подготовке к посадке виноградника, самой посадки  до общего правильного ухода за ним
                </p>
                <div class="levels-container">
                    <div class="levels-container-inner">
                        <div class="level-item first center" data-number="first">
                            01
                        </div>
                        <div class="level-item second center" data-number="second">
                            02
                        </div>
                        <div class="level-item third center" data-number="third">
                            03
                        </div>
                    </div>

                </div>
                <div class="levels-tab-container">
                    <div class="tab-wrapper first">
                        <div class="tab-wrapper-inner">
                            <div class="tap-title">
                                Подготовка к посадке виноградника
                            </div>
                            <ul class="tap-info-box">
                                <li>
                                    Оценка микроклимата участка. Определение биоклиматических индексов и  перспективных к выращиванию сортовых групп. Формирование концепции сортового и клонового состава с учетом анализа климатических данных и маркетинговых перспектив портфеля продукции на рынке РФ.
                                </li>
                                <li>
                                    Оценка потенциала терруара, изучение почвенных профилей и анализов почвы. Определение гетерогенности поля, распределение на клетки по почвенным видам. Определение подвоев.
                                </li>
                                <li>
                                    Фитосанитарные и микробиологические обследования участка. Генетическая идентификация фитопатогенов
                                <li>
                                <li>
                                    Определение параметров для проектирования виноградника: плотность посадки, ориентация рядов, тип шпалеры, формировка кустов. Сопровождение и проектирования винорадника и технический надзор за проектной организацией
                                </li>
                            </ul>
                        </div>
                        <p class="how-it-works-content">
                            Теперь вы знаете сорто/клоно/подвойную комбинацию для каждой клетки с учетом целевых качественно/количественных показателей урожайности и  ассортимента тихих и игристых вин которые планируете выпускать. Вы готовы высаживать виноград и получать государственные субсидии на компенсацию  затрат на раскорчевку, подготовку почвы, покупку саженцев и посадку.
                        </p>
                    </div>
                    <div class="tab-wrapper second">
                        <div class="tab-wrapper-inner">
                            <div class="tap-title">
                                 Посадка виноградника
                            </div>
                            <ul class="tap-info-box">
                                <li>
                                    Подготовка почвы и посадка виноградника. Расчистка участка от старых виноградных насаждений, уборка шпалерного кола и проволоки, мульчирование, корчевание корней, плантаж, глубокое рыхление, дискование. Высадка сидератов и удобрение почвы. Подготовка саженцев к посадке и высадка саженцев современными посадочными машинами                                </li>
                                <li>
                                    Выдача рекомендаций, составление технологической карты, сопровождение работ по уходу за виноградником в первый год вегетации.                                 </li>
                                <li>
                                    Составление шорт-листа поставщиков шпалеры. Сопровождение (исполнение) процесса установки шпалеры.                                <li>
                                <li>
                                    Сопровождение подготовки и подача пакета документов для субсидирования посадки, ухода в первый год за виноградником и установки шпалеры.                                </li>
                            </ul>
                        </div>
                        <p class="how-it-works-content">
                            Мы много работали и теперь у вас высажен виноградник, установлена шпалера, получена госудраственная субсидия.                        </p>
                    </div>
                    <div class="tab-wrapper third">
                        <div class="tab-wrapper-inner">
                            <div class="tap-title">
                                Уход за виноградником
                            </div>
                            <ul class="tap-info-box">
                                <li>
                                    Выдача рекомендаций и сопровождение работ по уходу за виноградником во второй и
                                    последующие года вегетации: обрезка, зеленые операции, защита растений, контроль
                                    вызревания и определение даты сбора урожая
                                </li>
                            </ul>
                        </div>
                        <p class="how-it-works-content">
                            Виноградник выводится на стадию плодоношения и обслуживается с учетом целевого качества винограда.                         </p>
                    </div>
                </div>
            </div>
        </section>
        <section class="technical-park-section wrapper">
            <div class="technical-park-inner">
                <div class="technical-park-left">
                    <h6>
                        Илион Агро
                    </h6>
                    <h2>
                        Парк техники
                    </h2>
                    <div class="technical-items-box">
                        @foreach($park_cars as $key => $car)
                        <div class="technical-item" data-index={{$key}}>
                            {{$car->name}}
                        </div>
                        @endforeach
                    </div>
                </div>
                <div class="technical-park-right">
                    @foreach($park_cars as $key => $car)
                    <div class="technical-image-item" data-index={{$key}}>
                        <img src="{{$car->getMainImage()}}" alt="atest">
                    </div>
                    @endforeach
                </div>
            </div>
        </section>
        <section class="contact-us-section" id='contact-us'>
            <h2>Связаться с нами</h2>
            <form action="{{route('user-question')}}" method="POST" class="faq-question-container">
                    @csrf
                    <input name='email' type="email" id="contact_email" placeholder="Email">
                    <button>Отправить</button>
                <span style="color: green" class="success"></span>
                </form>
        </section>
        <footer class="footer-wrapper">
            <div class="footer-inner">
                <div>
                    <a href="#" class="footer-logo">
                        <img src="{{asset('/img/white-logo.svg')}}" alt="footer-logo" />
            {{--                    <span> ИЛИОН </span>--}}
                    </a>
                    <a href="#" class="footer-copyright-title first">
                        Политика конфиденциальности
                    </a>
                    <a href="#" class="footer-copyright-title">
                        Защита персональных данных
                    </a>
                    <p href="#" class="footer-copyright-title">
                    &#169; Винодел Крым, 2021. Все права защищены.
                    </p>
                </div>
                    
                <nav class="footer-navigation-wrapper">
                    <ul class="footer-navigation-box">
                        <li class="footer-navigation-item">
                            <a href="tel:{{$contact->phone}}"
                            ><img src="{{asset('/images/phone-icon.png')}}" alt="phone-icon"
                                /></a>
                        </li>
                        <li class="footer-navigation-item">
                            <a href="{{$contact->instagram}}">
                                <img src="{{asset('/images/instagram-icon.png')}}" alt="phone-icon"/>
                            </a>
                        </li>
                        <li class="footer-navigation-item">
                            <a href="https://wa.me/{{$contact->getValidNumber()}}" target="_blank"
                            ><img src="{{asset('/images/wats-up-icon.png')}}" alt="phone-icon"
                                /></a>
                        </li>
                        <li class="footer-navigation-item">
                            <a href="{{$contact->youtube}}"><img src="{{asset('/images/you-tube-icon.png')}}" alt="phone-icon"/></a>
                        </li>
                    </ul>
                </nav>
            </div>
        </footer>
    </div>
</div>
<script
        type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"
></script>
<script src="{{asset('/js/script-offer.js')}}"></script>
</body>
</html>
