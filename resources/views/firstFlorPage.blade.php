@extends('layout.other-header')
@section('title')
    {{$static_page->meta_title}}
@stop

@section('description')
    {{$static_page->meta_description}}
@stop

@section('keywords')
    {{$static_page->meta_keywords}}
@stop
@section('content')
    <link rel="stylesheet" href="{{asset('/styles/reset.css')}}"/>

    <section class="product-main-section">
        <ul class="pagination-wrapper">
            <li>
                <a href="{{route('main')}}">Главная </a>
            </li>
            <li>
                <a href="{{route('winemaking')}}">Виноградарство</a>
            </li>
            <li>
                <a>План первого этапа</a>
            </li>
        </ul>
        <div class="inner-page-container">
            <div class="inner-page-container-bg">
            </div>
            <h3 class="timplate-page-title">{{$static_page->title}}</h3>
            <br>
            <div class="inner-content-container" style="font: revert!important;">
                <div style="" class="inner-content-description padding">
                    {!! $static_page->description_block1 !!}
                </div>
                <div class="initiate-conent-box">
                    <div class="inner-content-description">
                        {!! $static_page->description_block2 !!}
                    </div>
                    <div class="inner-content-description">
                        {!! $static_page->description_block3 !!}
                    </div>
                </div>
            </div>
            <div class="workers-list-wrapper owners-list">
                <div class="worker-item-box">
                    <div class="worker-image">
                        <img src="{{asset('/images/eduard.jpg')}}" alt="">
                    </div>
                    <div class="vertical-devider">
                    </div>
                    <div class="worker-content-box">
                        <h6> {{ (new \App\Support\DynamicSettings())->getFIO()  }}</h6>
                        <p> {{ (new \App\Support\DynamicSettings())->getPosition()  }}</p>
                        <p class='black-description'>
                            {{ (new \App\Support\DynamicSettings())->getDescription()  }}
                        </p>
                        <a href="tel: {{ (new \App\Support\DynamicSettings())->getPhone()  }}"> <img src="{{asset('/images/phone-call.png')}}" alt="phone-call">  {{ (new \App\Support\DynamicSettings())->getPhone()  }}</a>
                        <a href="mailto:  {{ (new \App\Support\DynamicSettings())->getEmail()  }}"> <img src="{{asset('/images/mail-icon.png')}}" alt="phone-call">  {{ (new \App\Support\DynamicSettings())->getEmail()  }}</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop
