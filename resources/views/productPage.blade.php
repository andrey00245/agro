@section('title')
    {{$land->meta_title}}
@stop

@section('description')
    {{$land->meta_description}}
@stop

@section('keywords')
    {{$land->meta_keywords}}
@stop
@extends('layout.other-header')
@section('content')
    <link rel="stylesheet" type="text/css" href="{{'/slick/slick-theme.css'}}"/>
    <link rel="stylesheet" href="{{asset('/lightbox.css')}}"/>

    <section class="product-main-section">
        <ul class="pagination-wrapper">
            <li>
                <a href="{{route('main')}}">Главная </a>
            </li>
            <li>
                <a href="{{route('lands')}}">Земельные участки</a>
            </li>
            <li>
                <a>{{$land->title}}</a>
            </li>
        </ul>
        <h3 class="adaptive-title">{{$land->title}}</h3>
        <div class="product-main-inner">
            <div class="product-main-slider-wrapper">
                <div class="product-main-slider-container">
                    <div class="slider-for">
                        <a href="{{$land->getMainImage()}}" data-lightbox="image-1" data-title="My caption"><img src="{{$land->getMainImage()}}" alt=""/></a>
                        @if($land->hasGallery())
                            @foreach($land->getGallery() as $gallery)
                                <a href="{{$gallery->getUrl()}}" data-lightbox="image-1" data-title="My caption"><img src="{{$gallery->getUrl()}}" alt=""/></a>
                            @endforeach
                        @endif
                    </div>
                    @if($land->hasGallery())
                        <div class="slider-nav">
                            <img src="{{$land->getMainImage()}}" alt=""/>
                            @foreach($land->getGallery() as $gallery)
                                <img src="{{$gallery->getUrl()}}" alt=""/>
                            @endforeach
                        </div>
                    @endif
                </div>
            </div>
            <div class="product-main-content-wrapper">
                <div class="product-main-content">
                    <h2 class="product-main-content-title">{{$land->title}}</h2>
                    <div class="product-main-sub-title">Площадь: {{$land->area}} Га</div>
                    <p class="product-main-description">
                        {{$land->description}}
                    </p>
                    <div class="product-main-price-title">Цена за обьект:
                        <span> {{$land->cost_for_object}} руб </span></div>
                    <div class="product-main-price-title">Цена за Га: <span> {{$land->cost}} руб </span></div>
                    <br>
                    <div class="product-main-price-title">Контакт: <span> <a href="tel:{{$land->phone}}"></a>{{$land->phone}} </span></div>

                    <div class="product-main-map-container">
                        {!! $land->map !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
    @if($land->video_url)
        <section class="product-video-section">
            <div class="product-video-inner">
                <iframe
                        src="{{$land->video_url}}"
                        width="100"
                        frameborder="0"
                        allowfullscreen
                ></iframe>
                <h5 class="product-video-title">{{$land->video_title}}</h5>
                <p class="product-video-sub-title">
                    {{$land->video_description}}
                </p>
                <div class="adaptive-product-map">
                    {!! $land->map !!}
                </div>
            </div>
        </section>
        @endif
        </div>
@stop
