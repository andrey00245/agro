@section('title')
    {{$goodKnow->meta_title}}
@stop

@section('description')
    {{$goodKnow->meta_description}}
@stop

@section('keywords')
    {{$goodKnow->meta_keywords}}
@stop

@extends('layout.other-header')
@section('content')
    <link rel="stylesheet" href="{{asset('/styles/reset.css')}}"/>

    <section class="product-main-section">
        <ul class="pagination-wrapper">
            <li>
                <a href="{{route('main')}}">Главная </a>
            </li>
            <li>
                <a href="{{route('lands')}}">Земельные участки</a>
            </li>
            <li>
                <a>Строительство на сельхозземле</a>
            </li>
            <li>
                <a href="{{route('goodKnow')}}">Полезно знать</a>
            </li>
        </ul>
        <div class="inner-page-container article-page">
            <h3 class="timplate-page-title">{{$goodKnow->title}}</h3>
            <div class="article-date">
                {{$goodKnow->created_at}}
            </div>
            <div class="article-organization">
                {{$goodKnow->edition}}
            </div>
            <div class="inner-content-container">
                <p class="inner-content-description padding">
                    {{$goodKnow->description_block1}}
                </p>
                <div class="initiate-conent-box">
                    <p class="inner-content-description">
                        {{$goodKnow->description_block2}}
                    </p>
                    <p class="inner-content-description">
                        {{$goodKnow->description_block3}}
                    </p>
                </div>
            </div>
        </div>
    </section>
@stop
