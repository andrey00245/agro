@extends('layout.other-header')
@section('content')
    <link rel="stylesheet" href="{{asset('/styles/reset.css')}}"/>

    <section class="document-main-section good-know-page">
        <ul class="pagination-wrapper">
            <li>
                <a href="{{route('main')}}">Главная </a>
            </li>
            <li>
                <a href="{{route('lands')}}">Земельные участки</a>
            </li>
            <li>
                <a href="{{url()->previous()}}">Строительство на сельхозземле</a>
            </li>
            <li>
                <a>Полезно знать</a>
            </li>
        </ul>
        <div class="document-main-inner">
            <h2 class="document-main-title">
                Полезно знать
            </h2>
            <div class="document-list-container">
                @foreach($goodKnows as $goodKnow)
                    <div class="document-item">
                        <a href="{{route('showGoodToKnow',['slug' => $goodKnow->slug])}}">
                            <div class="document-title">
                                {{$goodKnow->title}}
                            </div>
                        </a>
                        <p class="document-description">
                            {{$goodKnow->created_at}}
                        </p>
                        <p class="document-description">
                            {{$goodKnow->getShortDescription()}}
                        </p>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@stop
