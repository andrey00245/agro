@extends('layout.other-header')
@section('content')
    <link rel="stylesheet" href="{{asset('/styles/reset.css')}}"/>

    <section class="document-main-section">
        <ul class="pagination-wrapper">
            <li>
                <a href="{{route('main')}}">Главная </a>
            </li>
            <li>
                <a href="{{route('winemaking')}}">Виноградарство</a>
            </li>
            <li>
                <a>Документы</a>
            </li>
        </ul>
        <div class="document-main-inner">
            <h2 class="document-main-title">
                Документы
            </h2>
            <div class="document-list-container">
                @foreach($documents as $document)
                    <div class="document-item">
                        <div class="document-title">
                            {{$document->title}}
                        </div>
                        <p class="document-description">
                          {{$document->description}}
                        </p>
                      @if($document->getFile())
                        <a class="you-tube-icon" href="{{$document->getFile()}}" download>
                            <img src="{{asset('/images/download-icon.png')}}" alt="download-icon">
                        </a>
                     @endif
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@stop
