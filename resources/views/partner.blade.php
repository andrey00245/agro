@extends('layout.other-header')
@section('content')
    <link rel="stylesheet" href="{{asset('/styles/reset.css')}}"/>

    <section class="document-main-section">
        <ul class="pagination-wrapper">
            <li>
                <a href="{{route('main')}}">Главная </a>
            </li>
            <li>
                <a>Наши партнеры</a>
            </li>
        </ul>
        <div class="document-main-inner">
            <h2 class="document-main-title">
                Наши партнеры
            </h2>
            <div class="document-list-container partneers-list-box">
                @foreach($partners as $partner)
               <div class="partneers-item-box">
                    <div class="partneer-logo">
                        <img src="{{$partner->image}}" alt="partneer-logo">
                    </div>
                    <p class="partneer-title">
                        {{$partner->name}}
                    </p>
                    <p class="partneer-sub-title">
                        {{$partner->business_area}}
                    </p>
                    <p class="partneer-description">
                        {{$partner->description}}
                    </p>
                    <a href="{{$partner->url ?? '#'}}" class="partneer-link" target="_blank">
                        Перейти на сайт
                    </a>
               </div>     
                @endforeach
            </div>
            <div class="library-go-btn">
                <a href="{{route('lands')}}" class="go-to-library">Земельные участки</a>
            </div>
        </div>
    </section>
@stop
